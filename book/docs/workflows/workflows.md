# Main development

All code of the Unlock E-Infrastructure can be found here:

https://git.wur.nl/unlock

## Workflows

CWL: Common Workflow Language

Almost all standard computational analyses within UNLOCK are written and executed using the Common Workflow Language (CWL). An overview of complete workflows can be found on our WorkFlowHub Team page:
https://workflowhub.eu/projects/16#workflows


### Developing a workflow for UNLOCK

Workflows are written in the Common Workflow Language and is used within the UNLOCK infrastructure.
To mimick the unlock environment as close as possible you can start the following docker container from within the http://gitlab.com/m-unlock/cwl repository.

```docker run -it --entrypoint /bin/bash -v ~/.irods:/root/.irods -v `pwd`/unlock:/unlock -v `pwd`/cwls:/cwls -v `pwd`/yaml:/yaml munlock/base```

This will start the docker environment that is similar to the unlock infrastructure. 

The docker command will:

 - change the entry point from ./run.sh to a bash shell
 - mount the .irods folder from your home directory or create it to store your irods credentials when you exit the docker
 - will mount the unlock folder located in your current directory (binaries / databases are stored there)
 - will mount the yaml fodler in which some examples are stored

To execute cwltool directly it is important to setup the irods environment so files can be acquired from the data store

```
export irodsHost='HOST'
export irodsPort='1247'
export irodsUserName='USERNAME'
export irodsZone='ZONE'
export irodsAuthScheme='password'
export irodsHome='/tempZone/home/USERNAME'
export irodsCwd='/'
export irodsPassword='PASSWORD'
```

The to execute the workflow the following command can be used

`cwltool --preserve-entire-environment --no-container --outdir WORKFLOW --provenance WORKFLOW/PROVENANCE cwls/workflows/workflow_assembly_illumina_paired.cwl yaml/genomic.yaml`

This will start the cwltool and preserve the environmental variables (irods environmental variables are required) and since you are in a docker container will not start another container.
The workflow and the corresponding yaml file is required so the program knows which workflow to start and which arguments are needed.

Since the cwls and the yamls are mounted directly into the container and overwrites the already existing folders you can now easily modify the workflow on your computer and test the files directly from within the container.


<!-- ### Kubernetes -->
