# UNLOCK E-Infrastructure

<br />

The UNLOCK E-Infrastructure, a **FAIR data platform**, is a vital part of the enabling character of the UNLOCK facility.

The heart of the UNLOCK E-Infrastructure consists of two elements; SURFsara-hosted **iRODS** data management, long-term storage and computing using **containerized applications and workflows**.

For data handling UNLOCK has adopted the **Resource Description Framework (RDF)** data-model as it enables the integration of independently created resources in a semantically structured framework.

<br><br><br>

![The unlock compute infrastructure](./figures/infra_3.png)

