# Assay RNA

## FileNameForward
|||
|---|---|
|**Package**|RNA|
|**Item (rdfs:label)**|Forward filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_1.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|


## FileNameReverse
|||
|---|---|
|**Package**|RNA|
|**Item (rdfs:label)**|Reverse filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_2.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|


## Reference
|||
|---|---|
|**Package**|RNA|
|**Item (rdfs:label)**|Reference|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|GCA_000007565.2|
|**URL**|http://m-unlock.nl/ontology/reference|
