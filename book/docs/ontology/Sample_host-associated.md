# Sample host-associated

## alt
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|altitude|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000094|
|**Definition**|Altitude is a term used to identify heights of objects such as airplanes, space shuttles, rockets, atmospheric balloons and heights of places such as atmospheric layers and clouds. It is used to measure the height of an object which is above the earth‚Äôs surface. In this context, the altitude measurement is the vertical distance between the earth's surface above sea level and the sampled position in the air|


## ances_data
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|ancestral data|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|A/3*B|


## biol_stat
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|biological status|
|**Requirement**|Optional|
|**Value syntax**|[wild \| natural \| semi-natural \| inbred line \| breeder's line \| hybrid \| clonal selection \| mutant]|
|**Example**|natural|


## blood_press_diast
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host blood pressure diastolic|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## blood_press_syst
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host blood pressure systolic|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## chem_administration
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## depth
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|depth|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. For sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## elev
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## genetic_mod
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|genetic modification|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|aox1A transgenic|


## gravidity
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|gravidity|
|**Requirement**|Optional|
|**Value syntax**|{boolean};{timestamp}|
|**Example**|yes;due date:2018-05-11|


## host_age
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 days|
|**Preferred unit**|year, day, hour|
|**URL**|https://w3id.org/mixs/terms/0000255|
|**Definition**|Age of host at the time of sampling; relevant scale depends on species and study, e.g. Could be seconds for amoebae or centuries for trees|


## host_body_habitat
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host body habitat|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|nasopharynx|


## host_body_product
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host body product|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Portion of mucus [fma66938]|


## host_body_site
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host body site|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|gill [UBERON:0002535]|


## host_body_temp
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host body temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|15 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000274|
|**Definition**|Core body temperature of the host when sample was collected|


## host_color
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host color|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_common_name
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host common name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|human|


## host_diet
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host diet|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|herbivore|


## host_disease_stat
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host disease status|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|rabies [DOID:11260]|


## host_dry_mass
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host dry mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000257|
|**Definition**|Measurement of dry mass|


## host_family_relation
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host family relationship|
|**Requirement**|Optional|
|**Value syntax**|{text};{text}|
|**Example**|offspring;Mussel25|


## host_genotype
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host genotype|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|C57BL/6|


## host_growth_cond
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host growth conditions|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|https://academic.oup.com/icesjms/article/68/2/349/617247|


## host_height
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host height|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.1 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000264|
|**Definition**|The height of subject|


## host_last_meal
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host last meal|
|**Requirement**|Optional|
|**Value syntax**|{text};{duration}|
|**Example**|corn feed;P2H|


## host_length
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host length|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000256|
|**Definition**|The length of subject|


## host_life_stage
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host life stage|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|adult|


## host_phenotype
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host phenotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|elongated [PATO:0001154]|


## host_sex
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host sex|
|**Requirement**|Optional|
|**Value syntax**|[female \| hermaphrodite \| non-binary \| male \| transgender \| transgender (female to male) \| transgender (male to female)
  \| undeclared]|
|**Example**|non-binary|


## host_shape
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host shape|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|round|


## host_subject_id
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host subject id|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|MPI123|


## host_subspecf_genlin
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host subspecific genetic lineage|
|**Requirement**|Optional|
|**Value syntax**|{rank name}:{text}|
|**Example**|subvariety:glabrum|


## host_substrate
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host substrate|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|rock|


## host_symbiont
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|observed host symbionts|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|flukeworms|


## host_taxid
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host taxid|
|**Requirement**|Optional|
|**Value syntax**|{NCBI taxid}|
|**Example**|7955|


## host_tot_mass
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|host total mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000263|
|**Definition**|Total mass of the host at collection, the unit depends on host|


## misc_param
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## organism_count
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## perturbation
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## salinity
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_capt_status
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|sample capture status|
|**Requirement**|Optional|
|**Value syntax**|[active surveillance in response to an outbreak \| active surveillance not initiated by an outbreak \| farm sample \| market sample \| other]|
|**Example**|farm sample|


## samp_dis_stage
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|sample disease stage|
|**Requirement**|Optional|
|**Value syntax**|[dissemination \| growth and reproduction \| infection \| inoculation \| penetration \| other]|
|**Example**|infection|


## samp_store_dur
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## temp
|||
|---|---|
|**Package**|host-associated|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|
