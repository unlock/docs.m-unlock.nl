# Sample food-animal and animal feed

## lat_lon
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|geographic location (latitude and longitude)|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {float}|
|**Example**|50.586825 6.408977|


## geo_loc_name
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|geographic location (country and/or sea,region)|
|**Requirement**|Mandatory|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Maryland, Bethesda|


## collection_date
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|collection date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## env_broad_scale
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|broad-scale environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|forest biome [ENVO:01000174]|


## env_local_scale
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|local environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|litter layer [ENVO:01000338]|


## env_medium
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|environmental medium|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|soil [ENVO:00001998]|


## seq_meth
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sequencing method|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|454 Genome Sequencer FLX [OBI:0000702]|


## samp_size
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|amount or size of sample collected|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 liters|
|**Preferred unit**|liter, gram, cm^2|
|**URL**|https://w3id.org/mixs/terms/0000001|
|**Definition**|The total amount or size (volume (ml), mass (g) or area (m2) ) of sample collected.|


## samp_collect_device
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample collection device|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|swab, biopsy, niskin bottle, push core, drag swab [GENEPIO:0002713]|


## experimental_factor
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|experimental factor|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|time series design [EFO:0001779]|


## nucl_acid_ext
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|nucleic acid extraction|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://mobio.com/media/wysiwyg/pdfs/protocols/12888.pdf|


## organism_count
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## spikein_count
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike-in organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## samp_stor_temp
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius.|


## samp_stor_dur
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|milliliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## pool_dna_extracts
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|pooling of DNA extracts (if done)|
|**Requirement**|Optional|
|**Value syntax**|{boolean},{integer}|
|**Example**|yes, 5|


## temp
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling.|


## samp_stor_loc
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|freezer 5|


## samp_transport_cont
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample transport container|
|**Requirement**|Optional|
|**Value syntax**|[bottle \| cooler \| glass vial \| plastic vial \|  \| vendor supplied container]|
|**Example**|cooler|


## perturbation
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## coll_site_geo_feat
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|collection site geographic feature|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|grocery store [GENEPIO:0001020]|


## food_origin
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food product origin geographic location|
|**Requirement**|Mandatory|
|**Value syntax**|{term}:{term}, {text}|
|**Example**|USA:Delmarva, Peninsula|


## food_prod
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food production system characteristics|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|organic plant cultivation [FOODON:03530253]|


## food_product_type
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food product type|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|shrimp (peeled, deep-frozen) [FOODON:03317171]|


## food_source
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food source|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|giant tiger prawn [FOODON:03412612]|


## IFSAC_category
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|Interagency Food Safety Analytics Collaboration (IFSAC) category|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Plants:Produce:Vegetables:Herbs:Dried Herbs|


## intended_consumer
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|intended consumer|
|**Requirement**|Mandatory|
|**Value syntax**|{integer} \| {termLabel} {[termID]}|
|**Example**|9606 o rsenior as food consumer [FOODON:03510254]|


## samp_purpose
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|purpose of sampling|
|**Requirement**|Mandatory|
|**Value syntax**|[active surveillance in response to an outbreak \| active surveillance not initiated by an outbreak \| clinical trial \| cluster investigation \| environmental assessment \| farm sample \| field trial \| for cause \| industry internal investigation \| market sample \| passive surveillance \| population based studies \| research \| research and development] or {text}|
|**Example**|field trial|


## animal_am
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal antimicrobial|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|tetracycline|


## animal_am_dur
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal antimicrobial duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {day}|
|**Example**|3 days|


## animal_am_freq
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal antimicrobial frequency|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|once daily|


## animal_am_route
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal antimicrobial route of administration|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|oral-feed|


## animal_am_use
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal antimicrobial intended use|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|shipping fever|


## animal_body_cond
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal body condition|
|**Requirement**|Optional|
|**Value syntax**|[normal \| over conditioned \| under conditioned]|
|**Example**|under conditioned|


## animal_diet
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal source diet|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Hay [FOODON:03301763]|


## animal_feed_equip
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|animal feeding equipment|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|self feeding [EOL:0001645] \|  straight feed trough [EOL:0001661]|


## animal_group_size
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal group size|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|80|


## animal_housing
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|animal housing system|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|pen rearing system [EOL:0001636]|


## animal_sex
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food animal source sex category|
|**Requirement**|Optional|
|**Value syntax**|[castrated female \| castrated male \| intact female \| intact male]|
|**Example**|castrated male|


## bacterial_density
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|bacteria density|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 colony forming units per gram dry weight|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0001194|
|**Definition**|Number of bacteria in sample, as defined by bacteria density (http://purl.obolibrary.org/obo/GENEPIO_0000043).|


## cons_food_stor_dur
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food stored by consumer (storage duration)|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P5D|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001195|
|**Definition**|The storage duration of the food commodity by the consumer, prior to onset of illness or sample collection. Indicate the timepoint written in ISO 8601 format.|


## cons_food_stor_temp
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food stored by consumer (storage temperature)|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit} \| {text}|
|**Example**|4 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001196|
|**Definition**|Temperature at which food commodity was stored by the consumer, prior to onset of illness or sample collection.|


## cons_purch_date
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|purchase date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2019-11-30 00:00:00|
|**Preferred unit**|date and time|


## cons_qty_purchased
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|quantity purchased|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit}|
|**Example**|5 cans|


## cult_isol_date
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|culture isolation date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## cult_result
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|culture result|
|**Requirement**|Optional|
|**Value syntax**|[absent \| active \| inactive \| negative \| no \| present \| positive \| yes]|
|**Example**|positive|


## cult_result_org
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|culture result organism|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## cult_target
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|culture target microbial analyte|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## enrichment_protocol
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|enrichment protocol|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|BAM Chapter 4: Enumeration of Escherichia coli and the Coliform Bacteria|


## env_feature
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|environmental feature|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food processing building [ENVO:00003863] \| animal feed [ENVO:02000047]|


## food_additive
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food additive|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|xanthan gum [FOODON:03413321]|


## food_contact_surf
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food contact surface|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|cellulose acetate [FOODON:03500034]|


## food_contain_wrap
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food container or wrapping|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Plastic shrink-pack [FOODON:03490137]|


## food_cooking_proc
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food cooking process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food blanching [FOODON:03470175]|


## food_dis_point
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food distribution point geographic location|
|**Requirement**|Optional|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA:Delmarva, Peninsula|


## food_dis_point_city
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food distribution point geographic location (city)|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Atlanta[GAZ:00004445]|


## food_ingredient
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food ingredient|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|bean (whole) [FOODON:00002753]|


## food_pack_capacity
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food package capacity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|454 grams|
|**Preferred unit**|grams|
|**URL**|https://w3id.org/mixs/terms/0001208|
|**Definition**|The maximum number of product units within a package.|


## food_pack_integrity
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food packing medium integrity|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food packing medium compromised [FOODON:00002517]|


## food_pack_medium
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food packing medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|packed in fruit juice [FOODON:03480039]|


## food_preserv_proc
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food preservation process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food slow freezing [FOODON:03470128]|


## food_prior_contact
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|material of contact prior to food packaging|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|processed in stainless steel container [FOODON:03530081]|


## food_prod_synonym
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food product synonym|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|pinot gris|


## food_product_qual
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food product by quality|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|raw [FOODON:03311126]|


## food_quality_date
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food quality date|
|**Requirement**|Optional|
|**Value syntax**|[best by \| best if used by \| freeze by \|  \| use by]; date|
|**Example**|best by 2020-05-24|


## food_source_age
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food source age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P6M|
|**Preferred unit**|days|
|**URL**|https://w3id.org/mixs/terms/0001251|
|**Definition**|The age of the food source host organim. Depending on the type of host organism, age may be more appropriate to report in days, weeks, or years.|


## food_trace_list
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food traceability list category|
|**Requirement**|Optional|
|**Value syntax**|[cheeses-other than hard cheeses \| cucumbers \| crustaceans \| finfish-including smoked finfish \| fruits and vegetables-fresh cut \| herbs-fresh \| leafy greens-including fresh cut leafy greens \| melons \| mollusks-bivalves \| nut butter \| peppers \| ready to eat deli salads \| sprouts \| tomatoes \| tropical tree fruits \| shell eggs]|
|**Example**|tropical tree fruits|


## food_trav_vehic
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food shipping transportation vehicle|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|aircraft [ENVO:01001488] \| car [ENVO:01000605]|


## food_treat_proc
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|food treatment process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|gluten removal process [FOODON:03460750]|


## HACCP_term
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|Hazard Analysis Critical Control Points (HACCP) guide food safety term|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|tetrodotoxic poisoning [FOODON:03530249]|


## library_prep_kit
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|library preparation kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Illumina DNA Prep|


## lot_number
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|lot number|
|**Requirement**|Optional|
|**Value syntax**|{integer}, {text}|
|**Example**|1239ABC01A, split Cornish hens|


## microb_cult_med
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|microbiological culture medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|brain heart infusion agar [MICRO:0000566]|


## part_plant_animal
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|part of plant or animal|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|chuck [FOODON:03530021]|


## repository_name
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|repository name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|FDA CFSAN Microbiology Laboratories|


## samp_collect_method
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample collection method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|environmental swab sampling|


## samp_pooling
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample pooling|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|5uL of extracted genomic DNA from 5 leaves were pooled|


## samp_rep_biol
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|biological sample replicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|6 replicates|


## samp_rep_tech
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|technical sample replicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 replicates|


## This is the scientific role or category that the subject organism or material has with respect to an investigation. This field accepts terms listed under specimen source material category (http://purl.obolibrary.org/obo/GENEPIO_0001237 or http://purl.obolibrary.org/obo/OBI_0100051).
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample source material category|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|environmental (swab or sampling) [GENEPIO:0001732]|


## samp_stor_device
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample storage device|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Whirl Pak sampling bag [GENEPIO:0002122]|


## samp_stor_media
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample storage media|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|peptone water medium [MICRO:0000548]|


## samp_transport_cont
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample transport container|
|**Requirement**|Optional|
|**Value syntax**|[bottle \| cooler \| glass vial \| plastic vial \|  \| vendor supplied container]|
|**Example**|cooler|


## samp_transport_dur
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample transport duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P10D|
|**Preferred unit**|days|
|**URL**|https://w3id.org/mixs/terms/0001231|
|**Definition**|The duration of time from when the sample was collected until processed. Indicate the duration for which the sample was stored written in ISO 8601 format.|


## samp_transport_temp
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sample transport temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit} {text}|
|**Example**|4 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001232|
|**Definition**|Temperature at which sample was transported, e.g. -20 or 4 degree Celsius.|


## sequencing_kit
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sequencing kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NextSeq 500/550 High Output Kit v2.5 (75 Cycles)|


## sequencing_location
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|sequencing location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|University of Maryland Genomics Resource Center|


## serovar_or_serotype
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|serovar or serotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Escherichia coli strain O157:H7 [NCIT:C86883]|


## spikein_AMR
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|antimicrobial phenotype of spike-in bacteria|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{termLabel} {[termID]}|
|**Example**|wild type [ARO:3004432]|


## spikein_antibiotic
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike-in with antibiotics|
|**Requirement**|Optional|
|**Value syntax**|{text} {integer}|
|**Example**|Tetracycline at 5 mg/ml|


## spikein_growth_med
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike-in growth medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|LB broth [MCO:0000036]|


## spikein_metal
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike-in with heavy metals|
|**Requirement**|Optional|
|**Value syntax**|{text} {integer}|
|**Example**|Cd at 20 ppm|


## spikein_org
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike in organism|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502] \| 28901|


## spikein_serovar
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike-in bacterial serovar or serotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Escherichia coli strain O157:H7 [NCIT:C86883] \| 83334|


## spikein_strain
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|spike-in microbial strain|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|169963|


## study_design
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|study design|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|in vitro design [OBI:0001285]|


## study_inc_dur
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|study incubation duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001237|
|**Definition**|Sample incubation duration if unpublished or unvalidated method is used. Indicate the timepoint written in ISO 8601 format.|


## study_inc_temp
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|study incubation temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|37 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001238|
|**Definition**|Sample incubation temperature if unpublished or unvalidated method is used.|


## study_timecourse
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|time-course duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2 days post inoculation|
|**Preferred unit**|dpi|
|**URL**|https://w3id.org/mixs/terms/0001239|
|**Definition**|For time-course research studies involving samples of the food commodity, indicate the total duration of the time-course study.|


## study_tmnt
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|study treatment|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Factor A \| spike-in \| levels high, medium, low|


## timepoint
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|timepoint|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001173|
|**Definition**|Time point at which a sample or observation is made or taken from a biomaterial as measured from some reference point. Indicate the timepoint written in ISO 8601 format.|


## misc_param
|||
|---|---|
|**Package**|food-animal and animal feed|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|
