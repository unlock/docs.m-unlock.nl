# Sample human-oral

## chem_administration
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## ethnicity
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|ethnicity|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|native american|


## host_age
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|30 years|
|**Preferred unit**|year, day, hour|
|**URL**|https://w3id.org/mixs/terms/0000255|
|**Definition**|Age of host at the time of sampling; relevant scale depends on species and study, e.g. Could be seconds for amoebae or centuries for trees|


## host_body_mass_index
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host body-mass index|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|22 kilogram per square meter|
|**Preferred unit**|kilogram per square meter|
|**URL**|https://w3id.org/mixs/terms/0000317|
|**Definition**|Body mass index, calculated as weight/(height)squared|


## host_body_product
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host body product|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Portion of mucus [fma66938]|


## host_body_site
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host body site|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Epithelium of tongue [fma284658]|


## host_body_temp
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host body temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|36.5 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000274|
|**Definition**|Core body temperature of the host when sample was collected|


## host_diet
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host diet|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|high-fat|


## host_disease_stat
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host disease status|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|measles [DOID:8622]|


## host_family_relation
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host family relationship|
|**Requirement**|Optional|
|**Value syntax**|{text};{text}|
|**Example**|mother;ID298|


## host_genotype
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host genotype|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|ST1|


## host_height
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host height|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.75 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000264|
|**Definition**|The height of subject|


## host_last_meal
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host last meal|
|**Requirement**|Optional|
|**Value syntax**|{text};{duration}|
|**Example**|french fries;P5H30M|


## host_occupation
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host occupation|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|veterinary|


## host_phenotype
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host phenotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Tinnitus [HP:0000360]|


## host_pulse
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host pulse|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|65 beats per minute|
|**Preferred unit**|beats per minute|
|**URL**|https://w3id.org/mixs/terms/0000333|
|**Definition**|Resting pulse, measured as beats per minute|


## host_sex
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host sex|
|**Requirement**|Optional|
|**Value syntax**|[female \| hermaphrodite \| non-binary \| male \| transgender \| transgender (female to male) \| transgender (male to female)
  \| undeclared]|
|**Example**|non-binary|


## host_subject_id
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host subject id|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|MPI123|


## host_symbiont
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|observed host symbionts|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|flukeworms|


## host_tot_mass
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|host total mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|65 kilogram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000263|
|**Definition**|Total mass of the host at collection, the unit depends on host|


## ihmc_medication_code
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|IHMC medication code|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|810|


## medic_hist_perform
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|medical history performed|
|**Requirement**|Optional|
|**Value syntax**|{boolean}|
|**Example**|True|


## misc_param
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## nose_throat_disord
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|nose/mouth/teeth/throat disorder|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## organism_count
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## perturbation
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## salinity
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_store_dur
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## temp
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## time_last_toothbrush
|||
|---|---|
|**Package**|human-oral|
|**Item (rdfs:label)**|time since last toothbrushing|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P2H45M|
