# Investigation core

## InvestigationIdentifier
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Investigation identifier|
|**Requirement**|Mandatory|
|**Value syntax**|{id}{5,25}|
|**Example**|InvestigationID|
|**URL**|http://schema.org/identifier|
|**Definition**|Identifier corresponding to the investigation|


## InvestigationTitle
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Investigation title|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{10,}|
|**Example**|Title about the investigation|
|**URL**|http://schema.org/title|
|**Definition**|Title describing the investigation|


## InvestigationDescription
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Investigation description|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{50,}|
|**Example**|Description of the investigation|
|**URL**|http://schema.org/description|
|**Definition**|Description of the investigation|


## FirstName
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Firstname|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Jane|
|**URL**|http://schema.org/givenName|
|**Definition**|The given name of a person|


## LastName
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Lastname|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Doe|
|**URL**|http://schema.org/familyName|
|**Definition**|The family name of a person|


## Email
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Email address|
|**Requirement**|Mandatory|
|**Value syntax**|{email}|
|**Example**|jane.doe@wur.nl|
|**URL**|http://schema.org/email|
|**Definition**|The email address of a person|


## ORCID
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|ORCID|
|**Requirement**|Optional|
|**Value syntax**|{orcid}|
|**Example**|0000-0003-2125-060X|


## Organization
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Organization|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Wageningen|
|**URL**|http://schema.org/memberOf|
|**Definition**|Member of an organization|


## Department
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Department|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Laboratory of Systems and Synthetic Biology|
|**URL**|http://schema.org/department|
|**Definition**|The department this person belongs to|
