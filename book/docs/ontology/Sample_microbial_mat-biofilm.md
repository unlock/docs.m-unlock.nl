# Sample microbial mat/biofilm

## alkalinity
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|alkalinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 milligram per liter|
|**Preferred unit**|milliequivalent per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000421|
|**Definition**|Alkalinity, the ability of a solution to neutralize acids to the equivalence point of carbonate or bicarbonate|


## alkyl_diethers
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|alkyl diethers|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.005 mole per liter|
|**Preferred unit**|mole per liter|
|**URL**|https://w3id.org/mixs/terms/0000490|
|**Definition**|Concentration of alkyl diethers|


## aminopept_act
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|aminopeptidase activity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.269 mole per liter per hour|
|**Preferred unit**|mole per liter per hour|
|**URL**|https://w3id.org/mixs/terms/0000172|
|**Definition**|Measurement of aminopeptidase activity|


## ammonium
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|ammonium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.5 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000427|
|**Definition**|Concentration of ammonium in the sample|


## bacteria_carb_prod
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|bacterial carbon production|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2.53 microgram per liter per hour|
|**Preferred unit**|nanogram per hour|
|**URL**|https://w3id.org/mixs/terms/0000173|
|**Definition**|Measurement of bacterial carbon production|


## biomass
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|biomass|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|total;20 gram|
|**Preferred unit**|ton, kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000174|
|**Definition**|Amount of biomass; should include the name for the part of biomass measured, e.g. Microbial, total. Can include multiple measurements|


## bishomohopanol
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|bishomohopanol|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|14 microgram per liter|
|**Preferred unit**|microgram per liter, microgram per gram|
|**URL**|https://w3id.org/mixs/terms/0000175|
|**Definition**|Concentration of bishomohopanol|


## bromide
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|bromide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 parts per million|
|**Preferred unit**|parts per million|
|**URL**|https://w3id.org/mixs/terms/0000176|
|**Definition**|Concentration of bromide|


## calcium
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|calcium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.2 micromole per liter|
|**Preferred unit**|milligram per liter, micromole per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000432|
|**Definition**|Concentration of calcium in the sample|


## carb_nitro_ratio
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|carbon/nitrogen ratio|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.417361111|


## chem_administration
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## chloride
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|chloride|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5000 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000429|
|**Definition**|Concentration of chloride in the sample|


## chlorophyll
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|chlorophyll|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 milligram per cubic meter|
|**Preferred unit**|milligram per cubic meter, microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000177|
|**Definition**|Concentration of chlorophyll|


## depth
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|depth|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. For sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## diether_lipids
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|diether lipids|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|0.2 nanogram per liter|
|**Preferred unit**|nanogram per liter|
|**URL**|https://w3id.org/mixs/terms/0000178|
|**Definition**|Concentration of diether lipids; can include multiple types of diether lipids|


## diss_carb_dioxide
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|dissolved carbon dioxide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000436|
|**Definition**|Concentration of dissolved carbon dioxide in the sample or liquid portion of the sample|


## diss_hydrogen
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|dissolved hydrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.3 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000179|
|**Definition**|Concentration of dissolved hydrogen|


## diss_inorg_carb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|dissolved inorganic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2059 micromole per kilogram|
|**Preferred unit**|microgram per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000434|
|**Definition**|Dissolved inorganic carbon concentration in the sample, typically measured after filtering the sample using a 0.45 micrometer filter|


## diss_org_carb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|dissolved organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|197 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000433|
|**Definition**|Concentration of dissolved organic carbon in the sample, liquid portion of the sample, or aqueous phase of the fluid|


## diss_org_nitro
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|dissolved organic nitrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 micromole per liter|
|**Preferred unit**|microgram per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000162|
|**Definition**|Dissolved organic nitrogen concentration measured as; total dissolved nitrogen - NH4 - NO3 - NO2|


## diss_oxygen
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|dissolved oxygen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|175 micromole per kilogram|
|**Preferred unit**|micromole per kilogram, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000119|
|**Definition**|Concentration of dissolved oxygen|


## elev
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## glucosidase_act
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|glucosidase activity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 mol per liter per hour|
|**Preferred unit**|mol per liter per hour|
|**URL**|https://w3id.org/mixs/terms/0000137|
|**Definition**|Measurement of glucosidase activity|


## magnesium
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|magnesium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|52.8 micromole per kilogram|
|**Preferred unit**|mole per liter, milligram per liter, parts per million, micromole per kilogram|
|**URL**|https://w3id.org/mixs/terms/0000431|
|**Definition**|Concentration of magnesium in the sample|


## mean_frict_vel
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|mean friction velocity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.5 meter per second|
|**Preferred unit**|meter per second|
|**URL**|https://w3id.org/mixs/terms/0000498|
|**Definition**|Measurement of mean friction velocity|


## mean_peak_frict_vel
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|mean peak friction velocity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 meter per second|
|**Preferred unit**|meter per second|
|**URL**|https://w3id.org/mixs/terms/0000502|
|**Definition**|Measurement of mean peak friction velocity|


## methane
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|methane|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.15 micromole per liter|
|**Preferred unit**|micromole per liter, parts per billion, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000101|
|**Definition**|Methane (gas) amount or concentration at the time of sampling|


## misc_param
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## n_alkanes
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|n-alkanes|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|n-hexadecane;100 milligram per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000503|
|**Definition**|Concentration of n-alkanes; can include multiple n-alkanes|


## nitrate
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|nitrate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|65 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000425|
|**Definition**|Concentration of nitrate in the sample|


## nitrite
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|nitrite|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.5 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000426|
|**Definition**|Concentration of nitrite in the sample|


## nitro
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|nitrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|4.2 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000504|
|**Definition**|Concentration of nitrogen (total)|


## org_carb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.5 microgram per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000508|
|**Definition**|Concentration of organic carbon|


## org_matter
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|organic matter|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.75 milligram per cubic meter|
|**Preferred unit**|microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000204|
|**Definition**|Concentration of organic matter|


## org_nitro
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|organic nitrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|4 micromole per liter|
|**Preferred unit**|microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000205|
|**Definition**|Concentration of organic nitrogen|


## organism_count
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## part_org_carb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|particulate organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.92 micromole per liter|
|**Preferred unit**|microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000515|
|**Definition**|Concentration of particulate organic carbon|


## perturbation
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## petroleum_hydrocarb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|petroleum hydrocarbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000516|
|**Definition**|Concentration of petroleum hydrocarbon|


## ph
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## phaeopigments
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|phaeopigments|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|2.5 milligram per cubic meter|
|**Preferred unit**|milligram per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000180|
|**Definition**|Concentration of phaeopigments; can include multiple phaeopigments|


## phosphate
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|phosphate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.7 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000505|
|**Definition**|Concentration of phosphate|


## phosplipid_fatt_acid
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|phospholipid fatty acid|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|2.98 milligram per liter|
|**Preferred unit**|mole per gram, mole per liter|
|**URL**|https://w3id.org/mixs/terms/0000181|
|**Definition**|Concentration of phospholipid fatty acids; can include multiple values|


## potassium
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|potassium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|463 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000430|
|**Definition**|Concentration of potassium in the sample|


## pressure
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|pressure|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 atmosphere|
|**Preferred unit**|atmosphere|
|**URL**|https://w3id.org/mixs/terms/0000412|
|**Definition**|Pressure to which the sample is subject to, in atmospheres|


## redox_potential
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|redox potential|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|300 millivolt|
|**Preferred unit**|millivolt|
|**URL**|https://w3id.org/mixs/terms/0000182|
|**Definition**|Redox potential, measured relative to a hydrogen cell, indicating oxidation or reduction potential|


## salinity
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_store_dur
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## silicate
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|silicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000184|
|**Definition**|Concentration of silicate|


## sodium
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sodium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10.5 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000428|
|**Definition**|Sodium concentration in the sample|


## sulfate
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sulfate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000423|
|**Definition**|Concentration of sulfate in the sample|


## sulfide
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|sulfide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000424|
|**Definition**|Concentration of sulfide in the sample|


## temp
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## tot_carb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|total carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## tot_nitro_content
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|total nitrogen content|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## tot_org_carb
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|total organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## turbidity
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|turbidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.3 nephelometric turbidity units|
|**Preferred unit**|formazin turbidity unit, formazin nephelometric units|
|**URL**|https://w3id.org/mixs/terms/0000191|
|**Definition**|Measure of the amount of cloudiness or haziness in water caused by individual particles|


## water_content
|||
|---|---|
|**Package**|microbial mat/biofilm|
|**Item (rdfs:label)**|water content|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
