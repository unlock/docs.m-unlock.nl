# Sample air

## air particulate matter concentration
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|air_PM_concen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|PM2.5;10 microgram per cubic meter|
|**Preferred unit**|microgram per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000108|
|**Definition**|Concentration of substances that remain suspended in the air, and comprise mixtures of organic and inorganic substances (PM10 and PM2.5); can report multiple PM's by entering numeric values preceded by name of PM|


## alt
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|altitude|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000094|
|**Definition**|Altitude is a term used to identify heights of objects such as airplanes, space shuttles, rockets, atmospheric balloons and heights of places such as atmospheric layers and clouds. It is used to measure the height of an object which is above the earth's surface. In this context, the altitude measurement is the vertical distance between the earth's surface above sea level and the sampled position in the air|


## barometric_press
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|barometric pressure|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 millibar|
|**Preferred unit**|millibar|
|**URL**|https://w3id.org/mixs/terms/0000096|
|**Definition**|Force per unit area exerted against a surface by the weight of air above that surface|


## carb_dioxide
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|carbon dioxide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|410 parts per million|
|**Preferred unit**|micromole per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000097|
|**Definition**|Carbon dioxide (gas) amount or concentration at the time of sampling|


## carb_monoxide
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|carbon monoxide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.1 parts per million|
|**Preferred unit**|micromole per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000098|
|**Definition**|Carbon monoxide (gas) amount or concentration at the time of sampling|


## chem_administration
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]};{timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## elev
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## humidity
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|humidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 gram per cubic meter|
|**Preferred unit**|gram per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000100|
|**Definition**|Amount of water vapour in the air, at the time of sampling|


## methane
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|methane|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1800 parts per billion|
|**Preferred unit**|micromole per liter, parts per billion, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000101|
|**Definition**|Methane (gas) amount or concentration at the time of sampling|


## organism_count
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|(aerobic \| anaerobic \| other)|
|**Example**|aerobic|


## oxygen
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|oxygen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|600 parts per million|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000104|
|**Definition**|Oxygen (gas) amount or concentration at the time of sampling|


## perturbation
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## pollutants
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|pollutants|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|lead;0.15 microgram per cubic meter|
|**Preferred unit**|gram, mole per liter, milligram per liter, microgram per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000107|
|**Definition**|Pollutant types and, amount or concentrations measured at the time of sampling; can report multiple pollutants by entering numeric values preceded by name of pollutant|


## salinity
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_store_dur
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## solar_irradiance
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|solar irradiance|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.36 kilowatts per square meter per day|
|**Preferred unit**|kilowatts per square meter per day, ergs per square centimeter per second|
|**URL**|https://w3id.org/mixs/terms/0000112|
|**Definition**|The amount of solar energy that arrives at a specific area of a surface during a specific time interval|


## temp
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## ventilation_rate
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|ventilation rate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|750 cubic meter per minute|
|**Preferred unit**|cubic meter per minute, liters per second|
|**URL**|https://w3id.org/mixs/terms/0000114|
|**Definition**|Ventilation rate of the system in the sampled premises|


## ventilation_type
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|ventilation type|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Operable windows|


## volatile_org_comp
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|volatile organic compounds|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|formaldehyde;500 nanogram per liter|
|**Preferred unit**|microgram per cubic meter, parts per million, nanogram per liter|
|**URL**|https://w3id.org/mixs/terms/0000115|
|**Definition**|Concentration of carbon-based chemicals that easily evaporate at room temperature; can report multiple volatile organic compounds by entering numeric values preceded by name of compound|


## wind_direction
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|wind direction|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Northwest|


## wind_speed
|||
|---|---|
|**Package**|air|
|**Item (rdfs:label)**|wind speed|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|21 kilometer per hour|
|**Preferred unit**|meter per second, kilometer per hour|
|**URL**|https://w3id.org/mixs/terms/0000118|
|**Definition**|Speed of wind measured at the time of sampling|
