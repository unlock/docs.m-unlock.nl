# Sample food

## food_trav_mode
|||
|---|---|
|**Package**|food|
|**Item (rdfs:label)**|food shipping transportation method|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|train travel [GENEPIO:0001060]|
