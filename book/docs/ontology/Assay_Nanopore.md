# Assay Nanopore

## SequencingKit
|||
|---|---|
|**Package**|Nanopore|
|**Item (rdfs:label)**|Sequencing Kit|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Kit 12/Q20+, Ultra-Long|


## Flowcell
|||
|---|---|
|**Package**|Nanopore|
|**Item (rdfs:label)**|Flowcell|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|FLO-MIN106, FLO-PRO111|


## FileName
|||
|---|---|
|**Package**|Nanopore|
|**Item (rdfs:label)**|Filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|GXB01322_20181217_FAK35493_GA10000_sequencing_run_Run00014_MIN106_RBK004_46674_0.fast5|
|**URL**|http://m-unlock.nl/ontology/file|
