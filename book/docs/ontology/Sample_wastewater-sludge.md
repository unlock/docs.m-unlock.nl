# Sample wastewater/sludge

## alkalinity
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|alkalinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 milligram per liter|
|**Preferred unit**|milliequivalent per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000421|
|**Definition**|Alkalinity, the ability of a solution to neutralize acids to the equivalence point of carbonate or bicarbonate|


## biochem_oxygen_dem
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|biochemical oxygen demand|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## chem_administration
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## chem_oxygen_dem
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|chemical oxygen demand|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## depth
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|depth|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. For sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## efficiency_percent
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|efficiency percent|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## emulsions
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|emulsions|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## gaseous_substances
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|gaseous substances|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## indust_eff_percent
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|industrial effluent percent|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## inorg_particles
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|inorganic particles|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## misc_param
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## nitrate
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|nitrate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|65 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000425|
|**Definition**|Concentration of nitrate in the sample|


## org_particles
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|organic particles|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## organism_count
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## perturbation
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## ph
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## phosphate
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|phosphate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.7 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000505|
|**Definition**|Concentration of phosphate|


## pre_treatment
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|pre-treatment|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## primary_treatment
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|primary treatment|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## reactor_type
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|reactor type|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## salinity
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_store_dur
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## secondary_treatment
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|secondary treatment|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## sewage_type
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sewage type|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## sludge_retent_time
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sludge retention time|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## sodium
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|sodium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10.5 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000428|
|**Definition**|Sodium concentration in the sample|


## soluble_inorg_mat
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|soluble inorganic material|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## soluble_org_mat
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|soluble organic material|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## suspend_solids
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|suspended solids|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## temp
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## tertiary_treatment
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|tertiary treatment|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## tot_nitro
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|total nitrogen concentration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 micromole per liter|
|**Preferred unit**|microgram per liter, micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000102|
|**Definition**|Total nitrogen concentration of water samples, calculated by: total nitrogen = total dissolved nitrogen + particulate nitrogen. Can also be measured without filtering, reported as nitrogen|


## tot_phosphate
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|total phosphate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## wastewater_type
|||
|---|---|
|**Package**|wastewater/sludge|
|**Item (rdfs:label)**|wastewater type|
|**Requirement**|Optional|
|**Value syntax**|{text}|
