# Sample built environment

## abs_air_humidity
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|absolute air humidity|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|9 gram per gram|
|**Preferred unit**|gram per gram, kilogram per kilogram, kilogram, pound|
|**URL**|https://w3id.org/mixs/terms/0000122|
|**Definition**|Actual mass of water vapor - mh20 - present in the air water vapor mixture|


## address
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|address|
|**Requirement**|Optional|
|**Value syntax**|{integer}{text}|


## adj_room
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|adjacent rooms|
|**Requirement**|Optional|
|**Value syntax**|{text};{integer}|


## aero_struc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|aerospace structure|
|**Requirement**|Optional|
|**Value syntax**|(plane \| glider)|
|**Example**|plane|


## air_temp
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|air temperature|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|20 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000124|
|**Definition**|Temperature of the air at the time of sampling|


## amount_light
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|amount of light|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## arch_struc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|architectural structure|
|**Requirement**|Optional|
|**Value syntax**|(building \| shed \| home)|
|**Example**|shed|


## avg_dew_point
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|average dew point|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25.5 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000141|
|**Definition**|The average of dew point measures taken at the beginning of every hour over a 24 hour period on the sampling day|


## avg_occup
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|average daily occupancy|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|2|


## avg_temp
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|average temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|12.5 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000142|
|**Definition**|The average of temperatures taken at the beginning of every hour over a 24 hour period on the sampling day|


## bathroom_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|bathroom count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|1|


## bedroom_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|bedroom count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|2|


## build_docs
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|design, construction, and operation documents|
|**Requirement**|Optional|
|**Value syntax**|(building information model \| commissioning report \| complaint logs \| contract administration \| cost estimate \| janitorial schedules or logs \| maintenance plans \| schedule \| sections \| shop drawings \| submittals \| ventilation system \| windows)|
|**Example**|maintenance plans|


## build_occup_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|building occupancy type|
|**Requirement**|Mandatory|
|**Value syntax**|(office \| market \| restaurant \| residence \| school \| residential \| commercial \| low rise \| high rise \| wood framed \| office \| health care \| school \| airport \| sports complex)|
|**Example**|market|


## building_setting
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|building setting|
|**Requirement**|Mandatory|
|**Value syntax**|[urban \| suburban \| exurban \| rural]|
|**Example**|rural|


## built_struc_age
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|built structure age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|15|
|**Preferred unit**|year|
|**URL**|https://w3id.org/mixs/terms/0000145|
|**Definition**|The age of the built structure since construction|


## built_struc_set
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|built structure setting|
|**Requirement**|Optional|
|**Value syntax**|(urban \| rural)|
|**Example**|rural|


## built_struc_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|built structure type|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## carb_dioxide
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|carbon dioxide|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|410 parts per million|
|**Preferred unit**|micromole per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000097|
|**Definition**|Carbon dioxide (gas) amount or concentration at the time of sampling|


## ceil_area
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling area|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 square meter|
|**Preferred unit**|square meter|
|**URL**|https://w3id.org/mixs/terms/0000148|
|**Definition**|The area of the ceiling space within the room|


## ceil_cond
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling condition|
|**Requirement**|Optional|
|**Value syntax**|(new \| visible wear \| needs repair \| damaged \| rupture)|
|**Example**|damaged|


## ceil_finish_mat
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling finish material|
|**Requirement**|Optional|
|**Value syntax**|(drywall \| mineral fibre \| tiles \| PVC \| plasterboard \| metal \| fiberglass \| stucco \| mineral wool/calcium silicate \| wood)|
|**Example**|stucco|


## ceil_struc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling structure|
|**Requirement**|Optional|
|**Value syntax**|(wood frame \| concrete)|
|**Example**|concrete|


## ceil_texture
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling texture|
|**Requirement**|Optional|
|**Value syntax**|(crows feet \| crows-foot stomp \| double skip \| hawk and trowel \| knockdown \| popcorn \| orange peel \| rosebud stomp \| Santa-Fe texture \| skip trowel \| smooth \| stomp knockdown \| swirl)|
|**Example**|popcorn|


## ceil_thermal_mass
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling thermal mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## ceil_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling type|
|**Requirement**|Optional|
|**Value syntax**|(cathedral \| dropped \| concave \| barrel-shaped \| coffered \| cove \| stretched)|
|**Example**|coffered|


## ceil_water_mold
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ceiling signs of water/mold|
|**Requirement**|Optional|
|**Value syntax**|(presence of mold visible \| no presence of mold visible)|
|**Example**|presence of mold visible|


## cool_syst_id
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|cooling system identifier|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|12345|


## date_last_rain
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|date last rain|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11:T14:30Z|


## dew_point
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|dew point|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|22 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000129|
|**Definition**|The temperature to which a given parcel of humid air must be cooled, at constant barometric pressure, for water vapor to condense into water.|


## door_comp_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door type, composite|
|**Requirement**|Optional|
|**Value syntax**|(metal covered \| revolving \| sliding \| telescopic)|
|**Example**|revolving|


## door_cond
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door condition|
|**Requirement**|Optional|
|**Value syntax**|(damaged \| needs repair \| new \| rupture \| visible wear)|
|**Example**|new|


## door_direct
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door direction of opening|
|**Requirement**|Optional|
|**Value syntax**|(inward \| outward \| sideways)|
|**Example**|inward|


## door_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door location|
|**Requirement**|Optional|
|**Value syntax**|(north \| south \| east \| west)|
|**Example**|north|


## door_mat
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door material|
|**Requirement**|Optional|
|**Value syntax**|(aluminum \| cellular PVC \| engineered plastic \| fiberboard \| fiberglass \| metal \| thermoplastic alloy \| vinyl \| wood \| wood/plastic composite)|
|**Example**|wood|


## door_move
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door movement|
|**Requirement**|Optional|
|**Value syntax**|(collapsible \| folding \| revolving \| rolling shutter \| sliding \| swinging)|
|**Example**|swinging|


## door_size
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door area or size|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2.5 square meter|
|**Preferred unit**|square meter|
|**URL**|https://w3id.org/mixs/terms/0000158|
|**Definition**|The size of the door|


## door_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door type|
|**Requirement**|Optional|
|**Value syntax**|(composite \| metal \| wooden)|
|**Example**|wooden|


## door_type_metal
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door type, metal|
|**Requirement**|Optional|
|**Value syntax**|(collapsible \| corrugated steel \| hollow \| rolling shutters \| steel plate)|
|**Example**|hollow|


## door_type_wood
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door type, wood|
|**Requirement**|Optional|
|**Value syntax**|(bettened and ledged \| battened \| ledged and braced \| battened \| ledged and framed \| battened \| ledged, braced and frame \| framed and paneled \| glashed or sash \| flush \| louvered \| wire gauged)|
|**Example**|battened|


## door_water_mold
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|door signs of water/mold|
|**Requirement**|Optional|
|**Value syntax**|(presence of mold visible \| no presence of mold visible)|
|**Example**|presence of mold visible|


## drawings
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|drawings|
|**Requirement**|Optional|
|**Value syntax**|(operation \| as built \| construction \| bid \| design \| building navigation map \| diagram \| sketch)|
|**Example**|sketch|


## elevator
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|elevator count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|2|


## escalator
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|escalator count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|4|


## exp_duct
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|exposed ductwork|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## exp_pipe
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|exposed pipes|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## ext_door
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|exterior door count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## ext_wall_orient
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|orientations of exterior wall|
|**Requirement**|Optional|
|**Value syntax**|(north \| south \| east \| west \| northeast \| southeast \| southwest \| northwest)|
|**Example**|northwest|


## ext_window_orient
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|orientations of exterior window|
|**Requirement**|Optional|
|**Value syntax**|(north \| south \| east \| west \| northeast \| southeast \| southwest \| northwest)|
|**Example**|southwest|


## filter_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|filter type|
|**Requirement**|Mandatory|
|**Value syntax**|(particulate air filter \| chemical air filter \| low-MERV pleated media \| HEPA \| electrostatic \| gas-phase or ultraviolet air treatments)|
|**Example**|HEPA|


## fireplace_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|fireplace type|
|**Requirement**|Optional|
|**Value syntax**|(gas burning \| wood burning)|
|**Example**|wood burning|


## floor_age
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## floor_area
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor area|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## floor_cond
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor condition|
|**Requirement**|Optional|
|**Value syntax**|(new \| visible wear \| needs repair \| damaged \| rupture)|
|**Example**|new|


## floor_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## floor_finish_mat
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor finish material|
|**Requirement**|Optional|
|**Value syntax**|(tile \| wood strip or parquet \| carpet \| rug \| laminate wood \| lineoleum \| vinyl composition tile \| sheet vinyl \| stone \| bamboo \| cork \| terrazo \| concrete \| none;specify unfinished \| sealed \| clear finish \| paint)|
|**Example**|carpet|


## floor_struc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor structure|
|**Requirement**|Optional|
|**Value syntax**|(balcony \| floating floor \| glass floor \| raised floor \| sprung floor \| wood-framed \| concrete)|
|**Example**|concrete|


## floor_thermal_mass
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor thermal mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## floor_water_mold
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|floor signs of water/mold|
|**Requirement**|Optional|
|**Value syntax**|(mold odor \| wet floor \| water stains \| wall discoloration \| floor discoloration \| ceiling discoloration \| peeling paint or wallpaper \| bulging walls \| condensation)|
|**Example**|ceiling discoloration|


## freq_clean
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|frequency of cleaning|
|**Requirement**|Optional|
|**Value syntax**|( Daily \|  Weekly \|  Monthly \|  Quarterly  \|  Annually \|  other)|


## freq_cook
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|frequency of cooking|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## furniture
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|furniture|
|**Requirement**|Optional|
|**Value syntax**|(cabinet \| chair \| desks)|
|**Example**|chair|


## gender_restroom
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|gender of restroom|
|**Requirement**|Optional|
|**Value syntax**|(all gender \| female \| gender neurtral \| male \| male and female \| unisex)|
|**Example**|male|


## hall_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|hallway/corridor count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## handidness
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|handidness|
|**Requirement**|Optional|
|**Value syntax**|(ambidexterity \| left handedness \| mixed-handedness \| right handedness)|
|**Example**|right handedness|


## heat_cool_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|heating and cooling system type|
|**Requirement**|Mandatory|
|**Value syntax**|(radiant system \| heat pump \| forced air system \| steam forced heat \| wood stove)|
|**Example**|heat pump|


## heat_deliv_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|heating delivery locations|
|**Requirement**|Optional|
|**Value syntax**|(north \| south \| east \| west)|
|**Example**|north|


## heat_sys_deliv_meth
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|heating system delivery method|
|**Requirement**|Optional|
|**Value syntax**|(conductive \| radiant)|
|**Example**|radiant|


## heat_system_id
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|heating system identifier|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## height_carper_fiber
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|height carpet fiber mat|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## indoor_space
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|indoor space|
|**Requirement**|Mandatory|
|**Value syntax**|(bedroom \| office \| bathroom \| foyer \| kitchen \| locker room \| hallway \| elevator)|
|**Example**|foyer|


## indoor_surf
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|indoor surface|
|**Requirement**|Epple|
|**Value syntax**|(cabinet \| ceiling \| counter top \| door \| shelving \| vent cover \| window \| wall)|
|**Example**|wall|


## inside_lux
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|inside lux light|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## int_wall_cond
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|interior wall condition|
|**Requirement**|Optional|
|**Value syntax**|(new \| visible wear \| needs repair \| damaged \| rupture)|
|**Example**|damaged|


## last_clean
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|last time swept/mopped/vacuumed|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11:T14:30Z|


## light_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|light type|
|**Requirement**|Mandatory|
|**Value syntax**|(natural light \| electric light \| desk lamp \| flourescent lights \| natural light \| none)|
|**Example**|desk lamp|


## max_occup
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|maximum occupancy|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## mech_struc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|mechanical structure|
|**Requirement**|Optional|
|**Value syntax**|(subway \| coach \| carriage \| elevator \| escalator \| boat \| train \| car \| bus)|
|**Example**|elevator|


## number_pets
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|number of pets|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## number_plants
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|number of houseplants|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## number_resident
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|number of residents|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## occup_density_samp
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|occupant density at sampling|
|**Requirement**|Mandatory|
|**Value syntax**|{float}|


## occup_document
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|occupancy documentation|
|**Requirement**|Optional|
|**Value syntax**|(automated count \| estimate \| manual count \| videos)|
|**Example**|estimate|


## occup_samp
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|occupancy at sampling|
|**Requirement**|Mandatory|
|**Value syntax**|{integer}|
|**Example**|10|


## organism_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Mandatory|
|**Value syntax**|{text};{float} {unit};(qPCR \| ATP \| MPN \| other)|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## pres_animal_insect
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|presence of pets, animals, or insects|
|**Requirement**|Optional|
|**Value syntax**|(cat \| dog \| rodent \| snake \| other);{integer}|
|**Example**|cat;5|


## quad_pos
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|quadrant position|
|**Requirement**|Optional|
|**Value syntax**|(North side \| West side \| South side \| East side)|
|**Example**|West side|


## rel_air_humidity
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|relative air humidity|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|0.8|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0000121|
|**Definition**|Partial vapor and air pressure, density of the vapor and air, or by the actual mass of the vapor and air|


## rel_humidity_out
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|outside relative humidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|12 per kilogram of air|
|**Preferred unit**|gram of air, kilogram of air|
|**URL**|https://w3id.org/mixs/terms/0000188|
|**Definition**|The recorded outside relative humidity value at the time of sampling|


## rel_samp_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|relative sampling location|
|**Requirement**|Optional|
|**Value syntax**|(edge of car \| center of car \| under a seat)|
|**Example**|center of car|


## room_air_exch_rate
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room air exchange rate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## room_architec_elem
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room architectural elements|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## room_condt
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room condition|
|**Requirement**|Optional|
|**Value syntax**|(new \| visible wear \| needs repair \| damaged \| rupture \| visible signs of mold/mildew)|
|**Example**|new|


## room_connected
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|rooms connected by a doorway|
|**Requirement**|Optional|
|**Value syntax**|(attic \| bathroom \| closet \| conference room \| elevator \| examining room \| hallway \| kitchen \| mail room \| office \| stairwell)|
|**Example**|office|


## room_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## room_dim
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room dimensions|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit} x {integer} {unit} x {integer} {unit}|


## room_door_dist
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room door distance|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit}|


## room_door_share
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|rooms that share a door with sampling room|
|**Requirement**|Optional|
|**Value syntax**|{text};{integer}|


## room_hallway
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|rooms that are on the same hallway|
|**Requirement**|Optional|
|**Value syntax**|{text};{integer}|


## room_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room location in building|
|**Requirement**|Optional|
|**Value syntax**|(corner room \| interior room \| exterior wall)|
|**Example**|interior room|


## room_moist_dam_hist
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room moisture damage or mold history|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## room_net_area
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room net area|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit}|


## room_occup
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room occupancy|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## room_samp_pos
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room sampling position|
|**Requirement**|Optional|
|**Value syntax**|(north corner \| south corner \| west corner \| east corner \| northeast corner \| northwest corner \| southeast corner \| southwest corner \| center)|
|**Example**|south corner|


## room_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room type|
|**Requirement**|Optional|
|**Value syntax**|(attic \| bathroom \| closet \| conference room \| elevator \| examining room \| hallway \| kitchen \| mail room \| private office \| open office \| stairwell \| ,restroom \| lobby \| vestibule \| mechanical or electrical room \| data center \| laboratory_wet \| laboratory_dry \| gymnasium \| natatorium \| auditorium \| lockers \| cafe \| warehouse)|
|**Example**|bathroom|


## room_vol
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room volume|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit}|


## room_wall_share
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|rooms that share a wall with sampling room|
|**Requirement**|Optional|
|**Value syntax**|{text};{integer}|


## room_window_count
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|room window count|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## samp_floor
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|sampling floor|
|**Requirement**|Optional|
|**Value syntax**|(1st floor \| 2nd floor \| {integer} floor \| basement \| lobby)|
|**Example**|4th floor|


## samp_room_id
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|sampling room ID or name|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## samp_sort_meth
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|sample size sorting method|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## samp_time_out
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|sampling time outside|
|**Requirement**|Optional|
|**Value syntax**|{float}|


## samp_weather
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|sampling day weather|
|**Requirement**|Optional|
|**Value syntax**|(clear sky \| cloudy \| foggy \| hail \| rain \| snow \| sleet \| sunny \| windy)|
|**Example**|foggy|


## season
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|season|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|autumn [NCIT:C94733]|


## season_use
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|seasonal use|
|**Requirement**|Optional|
|**Value syntax**|[Spring \| Summer \| Fall \| Winter]|
|**Example**|Winter|


## shad_dev_water_mold
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|shading device signs of water/mold|
|**Requirement**|Optional|
|**Value syntax**|[presence of mold visible \| no presence of mold visible]|
|**Example**|no presence of mold visible|


## shading_device_cond
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|shading device condition|
|**Requirement**|Optional|
|**Value syntax**|[damaged \| needs repair \| new \| rupture \| visible wear]|
|**Example**|new|


## shading_device_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|shading device location|
|**Requirement**|Optional|
|**Value syntax**|[exterior \| interior]|
|**Example**|exterior|


## shading_device_mat
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|shading device material|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## shading_device_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|shading device type|
|**Requirement**|Optional|
|**Value syntax**|[bahama shutters \| exterior roll blind \| gambrel awning \| hood awning \| porchroller awning \| sarasota shutters \| slatted aluminum \| solid aluminum awning \| sun screen \| tree \| trellis \| venetian awning]|
|**Example**|slatted aluminum awning|


## space_typ_state
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|space typical state|
|**Requirement**|Mandatory|
|**Value syntax**|[typically occupied \| typically unoccupied]|
|**Example**|typically occupied|


## specific
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|specifications|
|**Requirement**|Optional|
|**Value syntax**|[operation \| as built \| construction \| bid \| design \| photos]|
|**Example**|construction|


## specific_humidity
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|specific humidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|15 per kilogram of air|
|**Preferred unit**|gram of air, kilogram of air|
|**URL**|https://w3id.org/mixs/terms/0000214|
|**Definition**|The mass of water vapour in a unit mass of moist air, usually expressed as grams of vapour per kilogram of air, or, in air conditioning, as grains per pound.|


## substructure_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|substructure type|
|**Requirement**|Optional|
|**Value syntax**|[crawlspace \| slab on grade \| basement]|
|**Example**|basement|


## surf_air_cont
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|surface-air contaminant|
|**Requirement**|Epple|
|**Value syntax**|[dust \| organic matter \| particulate matter \| volatile organic compounds \| biological contaminants \| radon \| nutrients \| biocides]|
|**Example**|radon|


## surf_humidity
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|surface humidity|
|**Requirement**|Epple|
|**Value syntax**|{float} {unit}|
|**Example**|0.1|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0000123|
|**Definition**|Surfaces: water activity as a function of air and material moisture|


## surf_material
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|surface material|
|**Requirement**|Epple|
|**Value syntax**|[adobe \| carpet \| cinder blocks \| concrete \| hay bales \| glass \| metal \| paint \| plastic \| stainless steel \| stone \| stucco \| tile \| vinyl \| wood]|
|**Example**|wood|


## surf_moisture
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|surface moisture|
|**Requirement**|Epple|
|**Value syntax**|{float} {unit}|
|**Example**|0.01 gram per square meter|
|**Preferred unit**|parts per million, gram per cubic meter, gram per square meter|
|**URL**|https://w3id.org/mixs/terms/0000128|
|**Definition**|Water held on a surface|


## surf_moisture_ph
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|surface moisture pH|
|**Requirement**|Epple|
|**Value syntax**|{float}|
|**Example**|7|


## surf_temp
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|surface temperature|
|**Requirement**|Epple|
|**Value syntax**|{float} {unit}|
|**Example**|15 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000125|
|**Definition**|Temperature of the surface at the time of sampling|


## temp_out
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|temperature outside house|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000197|
|**Definition**|The recorded temperature value at sampling time outside|


## train_line
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|train line|
|**Requirement**|Optional|
|**Value syntax**|[red \| green \| orange]|
|**Example**|red|


## train_stat_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|train station collection location|
|**Requirement**|Optional|
|**Value syntax**|[south station above ground \| south station underground \| south station amtrak \| forest hills \| riverside]|
|**Example**|forest hills|


## train_stop_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|train stop collection location|
|**Requirement**|Optional|
|**Value syntax**|[end \| mid \| downtown]|
|**Example**|end|


## typ_occup_density
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|typical occupant density|
|**Requirement**|Mandatory|
|**Value syntax**|{float}|
|**Example**|25|


## ventilation_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|ventilation type|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Operable windows|


## wall_area
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall area|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## wall_const_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall construction type|
|**Requirement**|Optional|
|**Value syntax**|[frame construction \| joisted masonry \| light noncombustible \| masonry noncombustible \| modified fire resistive \| fire resistive]|
|**Example**|fire resistive|


## wall_finish_mat
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall finish material|
|**Requirement**|Optional|
|**Value syntax**|[plaster \| gypsum plaster \| veneer plaster \| gypsum board \| tile \| terrazzo \| stone facing \| acoustical treatment \| wood \| metal \| masonry]|
|**Example**|wood|


## wall_height
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall height|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## wall_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall location|
|**Requirement**|Optional|
|**Value syntax**|[north \| south \| east \| west]|
|**Example**|north|


## wall_surf_treatment
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall surface treatment|
|**Requirement**|Optional|
|**Value syntax**|[painted \| wall paper \| no treatment \| paneling \| stucco \| fabric]|
|**Example**|paneling|


## wall_texture
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall texture|
|**Requirement**|Optional|
|**Value syntax**|[crows feet \| crows-foot stomp \|  \| double skip \| hawk and trowel \| knockdown \| popcorn \| orange peel \| rosebud stomp \| Santa-Fe texture \| skip trowel \| smooth \| stomp knockdown \| swirl]|
|**Example**|popcorn|


## wall_thermal_mass
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall thermal mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## wall_water_mold
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|wall signs of water/mold|
|**Requirement**|Optional|
|**Value syntax**|[presence of mold visible \| no presence of mold visible]|
|**Example**|no presence of mold visible|


## water_feat_size
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|water feature size|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## water_feat_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|water feature type|
|**Requirement**|Optional|
|**Value syntax**|[fountain \| pool \| standing feature \| stream \| waterfall]|
|**Example**|stream|


## weekday
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|weekday|
|**Requirement**|Optional|
|**Value syntax**|[Monday \| Tuesday \| Wednesday \| Thursday \| Friday \| Saturday \| Sunday]|
|**Example**|Sunday|


## window_cond
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window condition|
|**Requirement**|Optional|
|**Value syntax**|[damaged \| needs repair \| new \| rupture \| visible wear]|
|**Example**|rupture|


## window_cover
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window covering|
|**Requirement**|Optional|
|**Value syntax**|[blinds \| curtains \| none]|
|**Example**|curtains|


## window_horiz_pos
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window horizontal position|
|**Requirement**|Optional|
|**Value syntax**|[left \| middle \| right]|
|**Example**|middle|


## window_loc
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window location|
|**Requirement**|Optional|
|**Value syntax**|[north \| south \| east \| west]|
|**Example**|west|


## window_mat
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window material|
|**Requirement**|Optional|
|**Value syntax**|[clad \| fiberglass \| metal \| vinyl \| wood]|
|**Example**|wood|


## window_open_freq
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window open frequency|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## window_size
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window area/size|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit} x {float} {unit}|


## window_status
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window status|
|**Requirement**|Optional|
|**Value syntax**|[closed \| open]|
|**Example**|open|


## window_type
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window type|
|**Requirement**|Optional|
|**Value syntax**|[single-hung sash window \| horizontal sash window \| fixed window]|
|**Example**|fixed window|


## window_vert_pos
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window vertical position|
|**Requirement**|Optional|
|**Value syntax**|[bottom \| middle \| top \| low \| middle \| high]|
|**Example**|middle|


## window_water_mold
|||
|---|---|
|**Package**|built environment|
|**Item (rdfs:label)**|window signs of water/mold|
|**Requirement**|Optional|
|**Value syntax**|[presence of mold visible \| no presence of mold visible]|
|**Example**|no presence of mold visible|
