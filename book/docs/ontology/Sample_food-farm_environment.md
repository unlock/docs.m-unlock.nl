# Sample food-farm environment

## lat_lon
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|geographic location (latitude and longitude)|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {float}|
|**Example**|50.586825 6.408977|


## geo_loc_name
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|geographic location (country and/or sea,region)|
|**Requirement**|Mandatory|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Maryland, Bethesda|


## collection_date
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|collection date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## env_broad_scale
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|broad-scale environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|forest biome [ENVO:01000174]|


## env_local_scale
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|local environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|litter layer [ENVO:01000338]|


## env_medium
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|environmental medium|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|soil [ENVO:00001998]|


## seq_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sequencing method|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|454 Genome Sequencer FLX [OBI:0000702]|


## samp_size
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|amount or size of sample collected|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 liters|
|**Preferred unit**|liter, gram, cm^2|
|**URL**|https://w3id.org/mixs/terms/0000001|
|**Definition**|The total amount or size (volume (ml), mass (g) or area (m2) ) of sample collected.|


## samp_collect_device
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample collection device|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|swab, biopsy, niskin bottle, push core, drag swab [GENEPIO:0002713]|


## nucl_acid_ext
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|nucleic acid extraction|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://mobio.com/media/wysiwyg/pdfs/protocols/12888.pdf|


## humidity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|humidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|30% relative humidity|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0000100|
|**Definition**|Amount of water vapour in the air, at the time of sampling.|


## organism_count
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## spikein_count
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike-in organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## samp_stor_temp
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius.|


## solar_irradiance
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|mean seasonal solar irradiance|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.36 kilowatts per square meter per day|
|**Preferred unit**|kilowatts per square meter per day, ergs per square centimeter per second|
|**URL**|https://w3id.org/mixs/terms/0000112|
|**Definition**|The amount of solar energy that arrives at a specific area of a surface during a specific time interval|


## ventilation_rate
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|ventilation rate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|750 cubic meter per minute|
|**Preferred unit**|cubic meter per minute, liters per second|
|**URL**|https://w3id.org/mixs/terms/0000114|
|**Definition**|Ventilation rate of the system in the sampled premises.|


## samp_stor_dur
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## wind_speed
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|wind speed|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.6 kilometers per hour|
|**Preferred unit**|kilometer per hour|
|**URL**|https://w3id.org/mixs/terms/0000118|
|**Definition**|speed of wind measured at the time of sampling.|


## salinity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|milliliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## previous_land_use
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|history/previous land use|
|**Requirement**|Optional|
|**Value syntax**|{text};{timestamp}|
|**Example**|fallow; 2018-05-11:T14:30Z|


## crop_rotation
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|history/crop rotation|
|**Requirement**|Optional|
|**Value syntax**|{boolean};{Rn/start_time/end_time/duration}|
|**Example**|yes;R2/2017-01-01/2018-12-31/P6M|


## soil_type_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil type method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|Frederick series; Weikert series|


## tot_org_c_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|total organic carbon method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://www.epa.gov/sites/production/files/20{1,15}12/documents/9060a.pdf|


## tot_nitro_cont_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|total nitrogen content method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://currentprotocols.onlinelibrary.wiley.com/doi/abs/10.1002/0471142913.fab0102s00|


## salinity_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|salinity method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|PMID: 22895776|


## host_age
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 days|
|**Preferred unit**|year, day, hour|
|**URL**|https://w3id.org/mixs/terms/0000255|
|**Definition**|Age of host at the time of sampling; relevant scale depends on species and study, e.g. Could be seconds for amoebae or centuries for trees.|


## host_dry_mass
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host dry mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000257|
|**Definition**|Measurement of dry mass.|


## host_height
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host height|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000264|
|**Definition**|The height of subject.|


## host_length
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host length|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000256|
|**Definition**|The length of subject.|


## host_tot_mass
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host total mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000263|
|**Definition**|Total mass of the host at collection, the unit depends on host.|


## root_med_carbon
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|rooting medium carbon|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|sucrose|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000577|
|**Definition**|Source of organic carbon in the culture rooting medium; e.g. sucrose.|


## root_med_macronutr
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|rooting medium macronutrients|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|KH2PO4;170¬†milligram per liter|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000578|
|**Definition**|Measurement of the culture rooting medium macronutrients (N,P, K, Ca, Mg, S); e.g. KH2PO4 (170¬†mg/L).|


## root_med_micronutr
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|rooting medium micronutrients|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|H3BO3;6.2¬†milligram per liter|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000579|
|**Definition**|Measurement of the culture rooting medium micronutrients (Fe, Mn, Zn, B, Cu, Mo); e.g. H3BO3 (6.2¬†mg/L).|


## depth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|depth|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. for sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## season_temp
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|mean seasonal air temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|18 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000643|
|**Definition**|Mean seasonal air temperature.|


## season_precpt
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|mean seasonal precipitation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|75 millimeters|
|**Preferred unit**|millimeter|
|**URL**|https://w3id.org/mixs/terms/0000645|
|**Definition**|The average of all seasonal precipitation values known, or an estimated equivalent value derived by such methods as regional indexes or Isohyetal maps.|


## tot_org_carb
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|total organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{percentage}|
|**Example**|0.02|
|**Preferred unit**|gram Carbon per kilogram sample material|
|**URL**|https://w3id.org/mixs/terms/0000533|
|**Definition**|Definition for soil: total organic carbon content of the soil, definition otherwise: total organic carbon content.|


## tot_nitro_content
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|total nitrogen content|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|35 milligrams Nitrogen per kilogram of soil|
|**Preferred unit**|milligrams Nitrogen per kilogram of soil|
|**URL**|https://w3id.org/mixs/terms/0000530|
|**Definition**|Total nitrogen content of the sample.|


## conduc
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|conductivity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 milliSiemens per centimeter|
|**Preferred unit**|milliSiemens per centimeter|
|**URL**|https://w3id.org/mixs/terms/0000692|
|**Definition**|Electrical conductivity of water.|


## turbidity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|turbidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.3 nephelometric turbidity units|
|**Preferred unit**|formazin turbidity unit, formazin nephelometric units, nephelometric turbidity units|
|**URL**|https://w3id.org/mixs/terms/0000191|
|**Definition**|Measure of the amount of cloudiness or haziness in water caused by individual particles.|


## size_frac_low
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|size-fraction lower threshold|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.2 micrometer|
|**Preferred unit**|micrometer|
|**URL**|https://w3id.org/mixs/terms/0000735|
|**Definition**|Refers to the mesh/pore size used to pre-filter/pre-sort the sample. Materials larger than the size threshold are excluded from the sample.|


## size_frac_up
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|size-fraction upper threshold|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|20 micrometer|
|**Preferred unit**|micrometer|
|**URL**|https://w3id.org/mixs/terms/0000736|
|**Definition**|Refers to the mesh/pore size used to retain the sample. Materials smaller than the size threshold are excluded from the sample.|


## temp
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling.|


## ventilation_type
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|ventilation type|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Operable windows|


## wind_direction
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|wind direction|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|0 degrees; Northwest|
|**Preferred unit**|degrees or cardinal direction|
|**URL**|https://w3id.org/mixs/terms/0000757|
|**Definition**|Wind direction is the direction from which a wind originates.|


## genetic_mod
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|genetic modification|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|aox1A transgenic|


## host_phenotype
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host phenotype|
|**Requirement**|Optional|
|**Value syntax**|{text};{termLabel} {[termID]}|
|**Example**|seed pod; green [PATO:0000320]|


## ph
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## ances_data
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|ancestral data|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|A/3*B|


## biotic_regm
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|biotic regimen|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|sample inoculated with Rhizobium spp. Culture|


## chem_administration
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## growth_habit
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|growth habit|
|**Requirement**|Optional|
|**Value syntax**|[erect \| semi-erect \| spreading \| prostrate]|
|**Example**|spreading|


## host_disease_stat
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host disease status|
|**Requirement**|Condition Specific|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|downy mildew|


## host_genotype
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host genotype|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Ts|


## host_taxid
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host taxid|
|**Requirement**|Optional|
|**Value syntax**|{NCBI taxid}|
|**Example**|4530|


## mechanical_damage
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|mechanical damage|
|**Requirement**|Optional|
|**Value syntax**|{text};{text}|
|**Example**|pruning;bark|


## perturbation
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## root_cond
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|rooting conditions|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|http://himedialabs.com/TD/PT158.pdf|


## root_med_ph
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|rooting medium pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.5|


## tillage
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|history/tillage|
|**Requirement**|Optional|
|**Value syntax**|[drill \| cutting disc \| ridge till \| strip tillage \| zonal tillage \| chisel \| tined \| mouldboard \| disc plough]|
|**Example**|chisel|


## ph_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|pH method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://www.epa.gov/sites/production/files/20{1,15}12/documents/9040c.pdf|


## growth_medium
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|growth medium|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|LB broth|


## season
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|season|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|autumn [NCIT:C94733]|


## food_product_type
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food product type|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|shrimp (peeled, deep-frozen) [FOODON:03317171]|


## samp_type
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample type (host or environmental context)|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|built environment sample [GENEPIO:0001248]|


## farm_water_source
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|farm watering water source|
|**Requirement**|Condition Specific|
|**Value syntax**|[freshwater \| brackish \| saline \| natural \| manmade \| estuary \| lake \| pond \| canal \| stream \| river \| melt pond \| well \| ditch \| reservior \| collected rainwater \| municipal \| storage tank]|
|**Example**|water well (ENVO:01000002)|


## plant_water_method
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|plant water delivery method|
|**Requirement**|Condition Specific|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|drip irrigation process [AGRO:00000056]|


## air_PM_concen
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|air particulate matter concentration|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|PM2.5;10 microgram per cubic meter|
|**Preferred unit**|micrograms per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000108|
|**Definition**|Concentration of substances that remain suspended in the air, and comprise mixtures of organic and inorganic substances (PM10 and PM2.5); can report multiple PM's by entering numeric values preceded by name of PM|


## animal_feed_equip
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|animal feeding equipment|
|**Requirement**|Optional|
|**Value syntax**|{termLabel}{[termID]}|
|**Example**|self feeding [EOL:0001645] \|  straight feed trough [EOL:0001661]|


## animal_intrusion
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|animal intrusion near sample source|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Thripidae [NCBITaxon:45053]|


## anim_water_method
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|animal water delivery method|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|water trough [EOL:0001618]|


## crop_yield
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|crop yield|
|**Requirement**|Optional|
|**Value syntax**|{float}{unit}|
|**Example**|570 kilogram per metre square|
|**Preferred unit**|kilogram per metre square|
|**URL**|https://w3id.org/mixs/terms/0001116|
|**Definition**|Amount of crop produced per unit or area of land.|


## cult_result
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|culture result|
|**Requirement**|Optional|
|**Value syntax**|[absent \| active \| inactive \| negative \| no \| present \| positive \| yes]|
|**Example**|positive|


## cult_result_org
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|culture result organism|
|**Requirement**|Optional|
|**Value syntax**|{TermLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## cult_target
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|culture target microbial analyte|
|**Requirement**|Optional|
|**Value syntax**|{TermLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## plant_part_maturity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|degree of plant part maturity|
|**Requirement**|Optional|
|**Value syntax**|{term label}{term ID}|
|**Example**|ripe or mature [FOODON:03530052]|


## adjacent_ecosystem
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|ecosystem(s) adjacent to farm|
|**Requirement**|Optional|
|**Value syntax**|{termLabel}{[termID]}|
|**Example**|estuarine biome [ENVO:01000020]|


## water_source_adjac
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|environmental feature adjacent water source|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|feedlot [ENVO:01000627]|


## farm_equip_shared
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|equipment shared with other farms|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|combine harvester [AGRO:00000473]|


## farm_equip_san
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|farm equipment sanitization|
|**Requirement**|Optional|
|**Value syntax**|{text} {float} {unit}|
|**Example**|hot water pressure wash, hypochlorite solution, 50 parts per million|
|**Preferred unit**|parts per million|
|**URL**|https://w3id.org/mixs/terms/0001124|
|**Definition**|Method used to sanitize growing and harvesting equipment. This can including type and concentration of sanitizing solution. Multiple terms can be separated by one or more pipes.|


## farm_equip_san_freq
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|farm equipment sanitization frequency|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Biweekly|


## farm_equip
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|farm equipment used|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|combine harvester [AGRO:00000473]|


## fertilizer_admin
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|fertilizer administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|fish emulsion [AGRO:00000082]|


## fertilizer_date
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|fertilizer administration date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## animal_group_size
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food animal group size|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|80|


## animal_diet
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food animal source diet|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Hay [FOODON_03301763]|


## food_contact_surf
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food contact surface|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|cellulose acetate [FOODON:03500034]|


## food_contain_wrap
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food container or wrapping|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Plastic shrink-pack [FOODON:03490137]|


## food_harvest_proc
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|Food harvesting process|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|hand-picked|


## food_pack_medium
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food packing medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|vacuum-packed [FOODON:03480027]|


## food_preserv_proc
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food preservation process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food slow freezing [FOODON:03470128]|


## food_prod_char
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food production characteristics|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|wild caught|


## food_trav_mode
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food shipping transportation method|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|train travel [GENEPIO:0001060]|


## food_trav_vehic
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|Food shipping transportation vehicle|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|aircraft [ENVO:01001488] \| car [ENVO:01000605]|


## food_source
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food source|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|giant tiger prawn [FOODON:03412612]|


## food_treat_proc
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food treatment process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|gluten removal process [FOODON:03460750]|


## extr_weather_event
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|extreme weather event|
|**Requirement**|Optional|
|**Value syntax**|[drought \| dust storm \| extreme cold \| extreme heat \| flood \| frost \| hail \| high precipitationhigh winds]|
|**Example**|hail|


## date_extr_weath
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|extreme weather date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11:T14:30Z|


## host_variety
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|host variety or cultivar|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|romaine lettuce; Athena melons; Patio Snacker cucumbers|


## intended_consumer
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|intended consumer|
|**Requirement**|Optional|
|**Value syntax**|{integer} \| {termLabel} {[termID]}|
|**Example**|9606 o rsenior as food consumer [FOODON:03510254]|


## library_prep_kit
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|library preparation kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Illumina DNA Prep|


## air_flow_impede
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|local air flow impediments|
|**Requirement**|Optional|
|**Value syntax**|[obstructed \| unobstructed]; {text}; {measurement value}|
|**Example**|obstructed;hay bales; 2 m|


## lot_number
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|lot number|
|**Requirement**|Optional|
|**Value syntax**|{integer}, {text}|
|**Example**|1239ABC01A, split Cornish hens|


## season_humidity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|mean seasonal humidity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.25|


## part_plant_animal
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|part of plant or animal|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|chuck [FOODON:03530021]|


## plant_growth_med
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|plant growth medium|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} or [husk \| other artificial liquid medium \| other artificial solid medium \| peat moss \| perlite \| pumice \| sand \| soil \| vermiculite \| water]|
|**Example**|soil|


## plant_reprod_crop
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|plant reproductive part|
|**Requirement**|Optional|
|**Value syntax**|[plant cutting \| pregerminated seed \| ratoon \| seed \| seedling \| whole mature plant]|
|**Example**|seedling|


## samp_purpose
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|purpose of sampling|
|**Requirement**|Optional|
|**Value syntax**|[active surveillance in response to an outbreak \| active surveillance not initiated by an outbreak \| clinical trial \| cluster investigation \| environmental assessment \| farm sample \| field trial \| for cause \| industry internal investigation \| market sample \| passive surveillance \| population based studies \| research \| research and development] or {text}|
|**Example**|field trial|


## repository_name
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|repository name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|FDA CFSAN Microbiology Laboratories|


## samp_collect_device
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample collection device|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|swab, biopsy, niskin bottle, push core, drag swab [GENEPIO:0002713]|


## samp_pooling
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample pooling|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|5uL of extracted genomic DNA from 5 leaves were pooled|


## samp_source_mat_cat
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sample source material category|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|environmental (swab or sampling) [GENEPIO:0001732]|


## sequencing_kit
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sequencing kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NextSeq 500/550 High Output Kit v2.5 (75 Cycles)|


## sequencing_location
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|sequencing location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|University of Maryland Genomics Resource Center|


## serovar_or_serotype
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|serovar or serotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Escherichia coli strain O157:H7 [NCIT:C86883]|


## soil_conductivity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil conductivity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 milliSiemens per centimeter|
|**Preferred unit**|milliSiemens per centimeter|
|**URL**|https://w3id.org/mixs/terms/0001158|
|**Definition**|Conductivity of soil at time of sampling.|


## soil_cover
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil cover|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|bare soil [ENVO01001616]|


## soil_pH
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## soil_rel_location
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil relative location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|furrow|


## soil_porosity
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil sediment porosity|
|**Requirement**|Optional|
|**Value syntax**|{percentage}|
|**Example**|0.2|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0001162|
|**Definition**|Porosity of soil or deposited sediment is volume of voids divided by the total volume of sample.|


## soil_temp
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil temperature|
|**Requirement**|Optional|
|**Value syntax**|{float}{unit}|
|**Example**|25 degrees Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001163|
|**Definition**|Temperature of soil at the time of sampling.|


## soil_texture_class
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil texture classification|
|**Requirement**|Optional|
|**Value syntax**|[clay \| clay loam \| loam \| loamy sand \| sand \| sandy clay \| sandy clay loam \| sandy loam \| silt \| silty clay \| silty clay loam \| silt loam]|
|**Example**|silty clay loam|


## soil_texture_meth
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil texture method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://uwlab.soils.wisc.edu/wp-content/uploads/sites/17/2015/09/particle_size.pdf (hydrometer method)|


## soil_type
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|soil type|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|plinthosol [ENVO:00002250]|


## spikein_org
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike in organism|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502] \| 28901|


## spikein_serovar
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike-in bacterial serovar or serotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Escherichia coli strain O157:H7 [NCIT:C86883] \| 83334|


## spikein_growth_med
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike-in growth medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|LB broth [MCO:0000036]|


## spikein_strain
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike-in microbial strain|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|169963|


## spikein_antibiotic
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike-in with antibiotics|
|**Requirement**|Optional|
|**Value syntax**|{text} {integer}|
|**Example**|Tetracycline at 5 mg/ml|


## spikein_metal
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|spike-in with heavy metals|
|**Requirement**|Optional|
|**Value syntax**|{text} {integer}|
|**Example**|Cd at 20 ppm|


## timepoint
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|timepoint|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001173|
|**Definition**|Time point at which a sample or observation is made or taken from a biomaterial as measured from some reference point. Indicate the timepoint written in ISO 8601 format.|


## water_frequency
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|water delivery frequency|
|**Requirement**|Optional|
|**Value syntax**|{float}{unit}|
|**Example**|2 per day|
|**Preferred unit**|per day, per week, per month|
|**URL**|https://w3id.org/mixs/terms/0001174|
|**Definition**|Number of water delivery events within a given period of time.|


## water_pH
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|water pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## water_source_shared
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|water source shared|
|**Requirement**|Optional|
|**Value syntax**|[multiple users, agricutural \| multiple users, other \| no sharing]|
|**Example**|no sharing|


## enrichment_protocol
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|enrichment protocol|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|BAM Chapter 4: Enumeration of Escherichia coli and the Coliform Bacteria|


## food_quality_date
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food quality date|
|**Requirement**|Optional|
|**Value syntax**|[best by \| best if used by \| freeze by \|  \| use by]; date|
|**Example**|best by 2020-05-24|


## IFSAC_category
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|Interagency Food Safety Analytics Collaboration (IFSAC) category|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Plants:Produce:Vegetables:Herbs:Dried Herbs|


## animal_housing
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|animal housing system|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|pen rearing system [EOL:0001636]|


## cult_isol_date
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|culture isolation date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## food_clean_proc
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|food cleaning process|
|**Requirement**|Optional|
|**Value syntax**|[drum and drain \| manual spinner \| rinsed with sanitizer solution \| rinsed with water \| scrubbed with brush \| scrubbed with hand \| soaking]|
|**Example**|rinsed with water \| scrubbed with brush|


## misc_param
|||
|---|---|
|**Package**|food-farm environment|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|
