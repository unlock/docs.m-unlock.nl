# Assay AmpliconLibrary

## ForwardBarcodeSequence
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Forward barcode sequence|
|**Requirement**|Mandatory|
|**Value syntax**|{dna}|
|**Example**|GGTACCAA|
|**URL**|http://m-unlock.nl/ontology/forwardBarcode|


## ReverseBarcodeSequence
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Reverse barcode sequence|
|**Requirement**|Mandatory|
|**Value syntax**|{dna}|
|**Example**|GGTAGAAT|
|**URL**|http://m-unlock.nl/ontology/reverseBarcode|


## FileNameForward
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Forward filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_1.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|


## FileNameReverse
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Reverse filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_2.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|


## ForwardPrimer
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Forward primer|
|**Requirement**|Mandatory|
|**Value syntax**|{dna}|
|**Example**|RGGATTAGATACCC|
|**URL**|http://m-unlock.nl/ontology/forwardPrimer|


## ReversePrimer
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Reverse primer|
|**Requirement**|Mandatory|
|**Value syntax**|{dna}|
|**Example**|CGACRRCCATGCANCACCT|
|**URL**|http://m-unlock.nl/ontology/reversePrimer|


## PrimerNames
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Primer names|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|784F-1064R|
|**URL**|http://m-unlock.nl/ontology/primerNames|


## target_subfragment
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Target subfragment|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|V4|
|**URL**|http://m-unlock.nl/ontology/target_subfragment|


## LibraryName
|||
|---|---|
|**Package**|AmpliconLibrary|
|**Item (rdfs:label)**|Library name|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|2016_0001|
