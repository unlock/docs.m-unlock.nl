# Sample miscellaneous natural or artificial environment

## alkalinity
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|alkalinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 milligram per liter|
|**Preferred unit**|milliequivalent per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000421|
|**Definition**|Alkalinity, the ability of a solution to neutralize acids to the equivalence point of carbonate or bicarbonate|


## alt
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|altitude|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000094|
|**Definition**|Altitude is a term used to identify heights of objects such as airplanes, space shuttles, rockets, atmospheric balloons and heights of places such as atmospheric layers and clouds. It is used to measure the height of an object which is above the earth‚Äôs surface. In this context, the altitude measurement is the vertical distance between the earth's surface above sea level and the sampled position in the air|


## ammonium
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|ammonium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.5 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000427|
|**Definition**|Concentration of ammonium in the sample|


## biomass
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|biomass|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|total;20 gram|
|**Preferred unit**|ton, kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000174|
|**Definition**|Amount of biomass; should include the name for the part of biomass measured, e.g. Microbial, total. Can include multiple measurements|


## bromide
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|bromide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 parts per million|
|**Preferred unit**|parts per million|
|**URL**|https://w3id.org/mixs/terms/0000176|
|**Definition**|Concentration of bromide|


## calcium
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|calcium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.2 micromole per liter|
|**Preferred unit**|milligram per liter, micromole per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000432|
|**Definition**|Concentration of calcium in the sample|


## chem_administration
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## chloride
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|chloride|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5000 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000429|
|**Definition**|Concentration of chloride in the sample|


## chlorophyll
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|chlorophyll|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 milligram per cubic meter|
|**Preferred unit**|milligram per cubic meter, microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000177|
|**Definition**|Concentration of chlorophyll|


## density
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|density|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1000 kilogram per cubic meter|
|**Preferred unit**|gram per cubic meter, gram per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000435|
|**Definition**|Density of the sample, which is its mass per unit volume (aka volumetric mass density)|


## depth
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|depth|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. For sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## diether_lipids
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|diether lipids|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|0.2 nanogram per liter|
|**Preferred unit**|nanogram per liter|
|**URL**|https://w3id.org/mixs/terms/0000178|
|**Definition**|Concentration of diether lipids; can include multiple types of diether lipids|


## diss_carb_dioxide
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|dissolved carbon dioxide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000436|
|**Definition**|Concentration of dissolved carbon dioxide in the sample or liquid portion of the sample|


## diss_hydrogen
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|dissolved hydrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.3 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000179|
|**Definition**|Concentration of dissolved hydrogen|


## diss_inorg_carb
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|dissolved inorganic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2059 micromole per kilogram|
|**Preferred unit**|microgram per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000434|
|**Definition**|Dissolved inorganic carbon concentration in the sample, typically measured after filtering the sample using a 0.45 micrometer filter|


## diss_org_nitro
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|dissolved organic nitrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 micromole per liter|
|**Preferred unit**|microgram per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000162|
|**Definition**|Dissolved organic nitrogen concentration measured as; total dissolved nitrogen - NH4 - NO3 - NO2|


## diss_oxygen
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|dissolved oxygen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|175 micromole per kilogram|
|**Preferred unit**|micromole per kilogram, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000119|
|**Definition**|Concentration of dissolved oxygen|


## elev
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## misc_param
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## nitrate
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|nitrate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|65 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000425|
|**Definition**|Concentration of nitrate in the sample|


## nitrite
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|nitrite|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.5 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000426|
|**Definition**|Concentration of nitrite in the sample|


## nitro
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|nitrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|4.2 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000504|
|**Definition**|Concentration of nitrogen (total)|


## org_carb
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.5 microgram per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000508|
|**Definition**|Concentration of organic carbon|


## org_matter
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|organic matter|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1.75 milligram per cubic meter|
|**Preferred unit**|microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000204|
|**Definition**|Concentration of organic matter|


## org_nitro
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|organic nitrogen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|4 micromole per liter|
|**Preferred unit**|microgram per liter|
|**URL**|https://w3id.org/mixs/terms/0000205|
|**Definition**|Concentration of organic nitrogen|


## organism_count
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## perturbation
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## ph
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## phosphate
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|phosphate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.7 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000505|
|**Definition**|Concentration of phosphate|


## phosplipid_fatt_acid
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|phospholipid fatty acid|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|2.98 milligram per liter|
|**Preferred unit**|mole per gram, mole per liter|
|**URL**|https://w3id.org/mixs/terms/0000181|
|**Definition**|Concentration of phospholipid fatty acids; can include multiple values|


## potassium
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|potassium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|463 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000430|
|**Definition**|Concentration of potassium in the sample|


## pressure
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|pressure|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 atmosphere|
|**Preferred unit**|atmosphere|
|**URL**|https://w3id.org/mixs/terms/0000412|
|**Definition**|Pressure to which the sample is subject to, in atmospheres|


## salinity
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_store_dur
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## silicate
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|silicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.05 micromole per liter|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000184|
|**Definition**|Concentration of silicate|


## sodium
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sodium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10.5 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000428|
|**Definition**|Sodium concentration in the sample|


## sulfate
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sulfate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000423|
|**Definition**|Concentration of sulfate in the sample|


## sulfide
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|sulfide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000424|
|**Definition**|Concentration of sulfide in the sample|


## temp
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## water_current
|||
|---|---|
|**Package**|miscellaneous natural or artificial environment|
|**Item (rdfs:label)**|water current|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 cubic meter per second|
|**Preferred unit**|cubic meter per second, knots|
|**URL**|https://w3id.org/mixs/terms/0000203|
|**Definition**|Measurement of magnitude and direction of flow within a fluid|
