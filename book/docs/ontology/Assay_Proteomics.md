# Assay Proteomics

## FileName
|||
|---|---|
|**Package**|Proteomics|
|**Item (rdfs:label)**|Filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|file.gz|
|**URL**|http://m-unlock.nl/ontology/file|


## Reference
|||
|---|---|
|**Package**|Proteomics|
|**Item (rdfs:label)**|Reference|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|GCA_000007565.2|
|**URL**|http://m-unlock.nl/ontology/reference|
