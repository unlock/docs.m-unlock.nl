# Assay PacBio

## FileNameForward
|||
|---|---|
|**Package**|PacBio|
|**Item (rdfs:label)**|Forward filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_1.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|
