# Sample food-food production facility

## lat_lon
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|geographic location (latitude and longitude)|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {float}|
|**Example**|50.586825 6.408977|


## geo_loc_name
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|geographic location (country and/or sea,region)|
|**Requirement**|Mandatory|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Maryland, Bethesda|


## collection_date
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|collection date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## env_broad_scale
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|broad-scale environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|forest biome [ENVO:01000174]|


## env_local_scale
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|local environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|litter layer [ENVO:01000338]|


## env_medium
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|environmental medium|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|soil [ENVO:00001998]|


## seq_meth
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sequencing method|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|454 Genome Sequencer FLX [OBI:0000702]|


## samp_size
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|amount or size of sample collected|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 liters|
|**Preferred unit**|liter, gram, cm^2|
|**URL**|https://w3id.org/mixs/terms/0000001|
|**Definition**|The total amount or size (volume (ml), mass (g) or area (m2) ) of sample collected.|


## samp_collect_device
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample collection device|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|biopsy, niskin bottle, push core|


## experimental_factor
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|experimental factor|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|time series design [EFO:EFO_0001779]|


## nucl_acid_ext
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|nucleic acid extraction|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://mobio.com/media/wysiwyg/pdfs/protocols/12888.pdf|


## organism_count
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## samp_stor_temp
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius.|


## samp_stor_dur
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## air_temp
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|air temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|4 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000124|
|**Definition**|Temperature of the air at the time of sampling.|


## room_dim
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|room dimensions|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit} x {integer} {unit} x {integer} {unit}|
|**Example**|4 meter x 4 meter x 4 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000192|
|**Definition**|The length, width and height of sampling room|


## freq_clean
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|frequency of cleaning|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Daily|


## samp_room_id
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sampling room ID or name|
|**Requirement**|Optional|
|**Value syntax**|{integer}|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|milliliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## pool_dna_extracts
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|pooling of DNA extracts (if done)|
|**Requirement**|Optional|
|**Value syntax**|{boolean},{integer}|
|**Example**|yes, 5|


## samp_stor_loc
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|freezer 5|


## surf_material
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|surface material|
|**Requirement**|Optional|
|**Value syntax**|[adobe \| carpet \| cinder blocks \| concrete \| hay bales \| glass \| metal \| paint \| plastic \| stainless steel \| stone \| stucco \| tile \| vinyl \| wood]|
|**Example**|wood|


## indoor_surf
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|indoor surface|
|**Requirement**|Optional|
|**Value syntax**|[cabinet \| ceiling \| counter top \| door \| shelving \| vent cover \| window \| wall]|
|**Example**|wall|


## avg_occup
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|average daily occupancy|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|6|


## samp_floor
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sampling floor|
|**Requirement**|Optional|
|**Value syntax**|[1st floor \| 2nd floor \| {integer} floor \| basement \| lobby]|
|**Example**|4th floor|


## genetic_mod
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|genetic modification|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|aox1A transgenic|


## coll_site_geo_feat
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|collection site geographic feature|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|grocery store [GENEPIO:0001020]|


## samp_source_mat_cat
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample source material category|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|environmental swab specimen [OBI:0002613]|


## samp_type
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample type|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|built environment sample [GENEPIO:0001248]|


## samp_stor_media
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample storage media|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|peptone water medium [MICRO:0000548]|


## samp_stor_device
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample storage device|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Whirl Pak sampling bag [GENEPIO:0002122]|


## food_product_type
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food product type|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|shrimp (peeled, deep-frozen) [FOODON:03317171]|


## IFSAC_category
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|Interagency Food Safety Analytics Collaboration (IFSAC) category|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Plants:Produce:Vegetables:Herbs:Dried Herbs|


## food_product_qual
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food product by quality|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|raw [FOODON:03311126]|


## food_contact_surf
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food contact surface|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|cellulose acetate [FOODON:03500034]|


## facility_type
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|facility type|
|**Requirement**|Optional|
|**Value syntax**|[ambient storage \| caterer-catering point \| distribution \| frozen storage \| importer-broker \| interstate conveyance \| labeler-relabeler \| manufacturing-processing \| packaging \| refrigerated storage \| storage]|
|**Example**|manufacturing-processing|


## food_trav_mode
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food shipping transportation method|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|train travel [GENEPIO:0001060]|


## food_trav_vehic
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food shipping transportation vehicle|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|aircraft [ENVO:01001488] \| car [ENVO:01000605]|


## samp_transport_dur
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample transport duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P10D|
|**Preferred unit**|days|
|**URL**|https://w3id.org/mixs/terms/0001231|
|**Definition**|The duration of time from when the sample was collected until processed. Indicate the duration for which the sample was stored written in ISO 8601 format.|


## samp_transport_temp
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample transport temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit} {text}|
|**Example**|4 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001232|
|**Definition**|Temperature at which sample was transported, e.g. -20 or 4 degree Celsius.|


## samp_collect_method
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample collection method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|environmental swab sampling|


## num_samp_collect
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|number of samples collected|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|116 samples|


## lot_number
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|lot number|
|**Requirement**|Optional|
|**Value syntax**|{integer}, {text}|
|**Example**|1239ABC01A, split Cornish hens|


## hygienic_area
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|hygienic food production area|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Low Hygiene Area|


## env_monitoring_zone
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food production environmental monitoring zone|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Zone 1|


## area_samp_size
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|area sampled size|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit} x {integer} {unit}|
|**Example**|12 centimeter x 12 centimeter|
|**Preferred unit**|centimeter|
|**URL**|https://w3id.org/mixs/terms/0001255|
|**Definition**|The total amount or size (volume (ml), mass (g) or area (m2) ) of sample collected.|


## samp_surf_moisture
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample surface moisture|
|**Requirement**|Optional|
|**Value syntax**|[intermittent moisture \| not present \| submerged]|
|**Example**|submerged|


## samp_loc_condition
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample location condition|
|**Requirement**|Optional|
|**Value syntax**|[damaged \| new \| rupture \| visible signs of mold-mildew \| visible weariness repair]|
|**Example**|new|


## biocide_used
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|biocide|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Quaternary ammonium compound \| SterBac|


## ster_meth_samp_room
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sampling room sterilization method|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|ultraviolet radiation [ENVO:21001216] \| infrared radiation [ENVO:21001214]|


## enrichment_protocol
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|enrichment protocol|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|BAM Chapter 4: Enumeration of Escherichia coli and the Coliform Bacteria|


## cult_target
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|culture target microbial analyte|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## microb_cult_med
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|microbiological culture medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|brain heart infusion agar [MICRO:0000566]|


## timepoint
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|timepoint|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001173|
|**Definition**|Time point at which a sample or observation is made or taken from a biomaterial as measured from some reference point. Indicate the timepoint written in ISO 8601 format.|


## bacterial_density
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|bacteria density|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 colony forming units per gram dry weight|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0001194|
|**Definition**|Number of bacteria in sample, as defined by bacteria density (http://purl.obolibrary.org/obo/GENEPIO_0000043).|


## cult_isol_date
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|culture isolation date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## cult_result
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|culture result|
|**Requirement**|Optional|
|**Value syntax**|[absent \| active \| inactive \| negative \| no \| present \| positive \| yes]|
|**Example**|positive|


## cult_result_org
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|culture result organism|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## subspecf_gen_lin
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|subspecific genetic lineage|


## samp_pooling
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample pooling|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|5uL of extracted genomic DNA from 5 leaves were pooled|


## samp_purpose
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|purpose of sampling|
|**Requirement**|Optional|
|**Value syntax**|[active surveillance in response to an outbreak \| active surveillance not initiated by an outbreak \| clinical trial \| cluster investigation \| environmental assessment \| farm sample \| field trial \| for cause \| industry internal investigation \| market sample \| passive surveillance \| population based studies \| research \| research and development] or {text}|
|**Example**|field trial|


## samp_rep_tech
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|technical sample replicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 replicates|


## samp_rep_biol
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|biological sample replicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|6 replicates|


## samp_transport_cont
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sample transport container|
|**Requirement**|Optional|
|**Value syntax**|[bottle \| cooler \| glass vial \| plastic vial \|  \| vendor supplied container]|
|**Example**|cooler|


## study_design
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|study design|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|in vitro design [OBI:0001285]|


## nucl_acid_ext_kit
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|nucleic acid extraction kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Qiagen PowerSoil Kit|


## library_prep_kit
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|library preparation kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Illumina DNA Prep|


## sequencing_kit
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sequencing kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NextSeq 500/550 High Output Kit v2.5 (75 Cycles)|


## sequencing_location
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|sequencing location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|University of Maryland Genomics Resource Center|


## study_inc_temp
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|study incubation temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|37 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001238|
|**Definition**|Sample incubation temperature if unpublished or unvalidated method is used.|


## study_inc_dur
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|study incubation duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001237|
|**Definition**|Sample incubation duration if unpublished or unvalidated method is used. Indicate the timepoint written in ISO 8601 format.|


## study_timecourse
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|time-course duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2 days post inoculation|
|**Preferred unit**|dpi|
|**URL**|https://w3id.org/mixs/terms/0001239|
|**Definition**|For time-course research studies involving samples of the food commodity, indicate the total duration of the time-course study.|


## study_tmnt
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|study treatment|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Factor A \| spike-in \| levels high, medium, low|


## food_source
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food source|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|giant tiger prawn [FOODON:03412612]|


## food_dis_point
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food distribution point geographic location|
|**Requirement**|Optional|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Delmarva, Peninsula|


## food_dis_point_city
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food distribution point geographic location (city)|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Atlanta[GAZ:00004445]|


## food_origin
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food product origin geographic location|
|**Requirement**|Optional|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Delmarva, Peninsula|


## food_prod_synonym
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food product synonym|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|pinot gris|


## food_additive
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food additive|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|xanthan gum [FOODON_03413321]|


## food_trace_list
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food traceability list category|
|**Requirement**|Optional|
|**Value syntax**|[cheeses-other than hard cheeses \| cucumbers \| crustaceans \| finfish-including smoked finfish \| fruits and vegetables-fresh cut \| herbs-fresh \| leafy greens-including fresh cut leafy greens \| melons \| mollusks-bivalves \| nut butter \| peppers \| ready to eat deli salads \| sprouts \| tomatoes \| tropical tree fruits \| shell eggs]|
|**Example**|tropical tree fruits|


## part_plant_animal
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|part of plant or animal|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|chuck [FOODON:03530021]|


## food_ingredient
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food ingredient|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|bean (whole) [FOODON:00002753]|


## spec_intended_cons
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|specific intended consumer|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|senior as food consumer [FOODON:03510254]|


## HACCP_term
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|Hazard Analysis Critical Control Points (HACCP) guide food safety term|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|tetrodotoxic poisoning [FOODON:03530249]|


## dietary_claim_use
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|dietary claim or use|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|No preservatives [FOODON:03510113]|


## food_allergen_label
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food allergen labeling|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food allergen labelling about crustaceans and products thereof [FOODON:03510215]|


## food_prod_char
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food production characteristics|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|wild caught|


## food_name_status
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food product name legal status|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|protected geographic indication [FOODON:03530256]|


## food_preserv_proc
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food preservation process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food slow freezing [FOODON:03470128]|


## food_cooking_proc
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food cooking process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food blanching [FOODON:03470175]|


## food_treat_proc
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food treatment process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|gluten removal process [FOODON:03460750]|


## food_contain_wrap
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food container or wrapping|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Plastic shrink-pack [FOODON:03490137]|


## food_pack_capacity
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food package capacity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|454 grams|
|**Preferred unit**|grams|
|**URL**|https://w3id.org/mixs/terms/0001208|
|**Definition**|The maximum number of product units within a package.|


## food_pack_medium
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food packing medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|vacuum-packed [FOODON:03480027]|


## food_prior_contact
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|material of contact prior to food packaging|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|processed in stainless steel container [FOODON:03530081]|


## food_prod
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food production system characteristics|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|organic plant cultivation [FOODON:03530253]|


## food_quality_date
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food quality date|
|**Requirement**|Optional|
|**Value syntax**|[best by \| best if used by \| freeze by \|  \| use by]; date|
|**Example**|best by 2020-05-24|


## repository_name
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|repository name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|FDA CFSAN Microbiology Laboratories|


## intended_consumer
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|intended consumer|
|**Requirement**|Optional|
|**Value syntax**|{integer} \| {termLabel} {[termID]}|
|**Example**|9606 o rsenior as food consumer [FOODON:03510254]|


## food_pack_integrity
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|food packing medium integrity|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food packing medium compromised [FOODON:00002517]|


## misc_param
|||
|---|---|
|**Package**|food-food production facility|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|
