# Assay Genome

## FileName
|||
|---|---|
|**Package**|Genome|
|**Item (rdfs:label)**|Filename|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|genome.fasta|
|**URL**|http://m-unlock.nl/ontology/file|
