# Sample core

## SampleIdentifier
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sample identifier|
|**Requirement**|Mandatory|
|**Value syntax**|{id}{5,25}|
|**Example**|P1S2D3|


## SampleDescription
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sample description|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{25,}|
|**Example**|The sample taken from pig 1 in stable 2 on day 3 was more colorless than in general|


## samp_name
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|sample name|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{10,}|
|**Example**|ISDsoil1|


## NCBI taxonomy ID
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|NCBI taxonomy ID|
|**Requirement**|Mandatory|
|**Value syntax**|{NCBI taxid}|
|**Example**|1510822|


## SampleOrganism
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sample organism|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Sus scrofa domesticus|


## BioSafetyLevel
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Biosafety level|
|**Requirement**|Mandatory|
|**Value syntax**|{integer}{1}|
|**Example**|2|


## SampleSize
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sample size|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 ml|


## SampleCondition
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sample condition|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|more coloreless|


## Material
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Material|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|gut sample|


## SamplingStrategy
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sampling strategy|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|strategy X|


## SampleTreatment
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Sample treatment|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|treatment X|


## TimePoint
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Time point|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|12 H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001173|
|**Definition**|Time point at which a sample or observation is made or taken from a biomaterial as measured from some reference point.|


## CollectionTemperature
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Collection temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|32.4 degrees celcius|
|**Preferred unit**|degree Celsius|


## CollectionPH
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Collection pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|6.1|


## lat_lon
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Latitude longitude|
|**Requirement**|Optional|
|**Value syntax**|{float} {float}|
|**Example**|50.586825 6.408977|


## depth
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|depth|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|Please refer to the definitions of depth in the environmental packages|


## alt
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|altitude|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000094|
|**Definition**|Altitude is a term used to identify heights of objects such as airplanes, space shuttles, rockets, atmospheric balloons and heights of places such as atmospheric layers and clouds. It is used to measure the height of an object which is above the earth‚Äôs surface. In this context, the altitude measurement is the vertical distance between the earth's surface above sea level and the sampled position in the air|


## elev
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## geo_loc_name
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|geographic location (country and/or sea,region)|
|**Requirement**|Optional|
|**Value syntax**|{term};{term};{text}|
|**Example**|Germany;North Rhine-Westphalia;Eifel National Park|


## collection_date
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|collection date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00|
|**Preferred unit**|yyyy-mm-ddThh:mm:ss|
|**URL**|https://w3id.org/mixs/terms/0000011|
|**Definition**|The time of sampling, either as an instance (single point in time) or interval. In case no exact time is available, the date/time can be right truncated i.e. all of these are valid times: 2008-01-23T19:23:{10,}00:00; 2008-01-23T19:23:10; 2008-01-23; 2008-01; 2008; Except: 2008-01; 2008 all are ISO8601 compliant|


## env_broad_scale
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|broad-scale environmental context|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|forest biome [ENVO:01000174]|


## env_local_scale
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|local environmental context|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|litter layer [ENVO:01000338]|


## env_medium
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|environmental medium|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|soil [ENVO:00001998]|
