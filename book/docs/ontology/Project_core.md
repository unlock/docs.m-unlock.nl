# Project core

## ProjectIdentifier
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Project identifier|
|**Requirement**|Mandatory|
|**Value syntax**|{id}{5,25}|
|**Example**|ProjectX|
|**URL**|http://schema.org/identifier|
|**Definition**|Identifier corresponding to the project|


## ProjectTitle
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Project title|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{10,}|
|**Example**|The title describing project X|
|**URL**|http://schema.org/title|
|**Definition**|Title describing the project|


## ProjectDescription
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Project description|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{50,}|
|**Example**|A longer description that explains what project X is all about.|
|**URL**|http://schema.org/description|
|**Definition**|Description of the project|
