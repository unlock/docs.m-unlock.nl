# Sample hydrocarbon resources-cores

## additional_info
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|additional info|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## alkalinity
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|alkalinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 milligram per liter|
|**Preferred unit**|milliequivalent per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000421|
|**Definition**|Alkalinity, the ability of a solution to neutralize acids to the equivalence point of carbonate or bicarbonate|


## alkalinity_method
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|alkalinity method|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|titration|


## ammonium
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|ammonium|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|1.5 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000427|
|**Definition**|Concentration of ammonium in the sample|


## api
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|API gravity|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|


## aromatics_pc
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|aromatics wt%|
|**Requirement**|Condition Specific|
|**Value syntax**|{text};{float} {unit}|


## asphaltenes_pc
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|asphaltenes wt%|
|**Requirement**|Condition Specific|
|**Value syntax**|{text};{float} {unit}|


## basin
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|basin name|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|


## benzene
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|benzene|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## calcium
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|calcium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.2 micromole per liter|
|**Preferred unit**|milligram per liter, micromole per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000432|
|**Definition**|Concentration of calcium in the sample|


## chloride
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|chloride|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5000 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000429|
|**Definition**|Concentration of chloride in the sample|


## density
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|density|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1000 kilogram per cubic meter|
|**Preferred unit**|gram per cubic meter, gram per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000435|
|**Definition**|Density of the sample, which is its mass per unit volume (aka volumetric mass density)|


## depos_env
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|depositional environment|
|**Requirement**|Condition Specific|
|**Value syntax**|[Continental - Alluvial \| Continental - Aeolian \| Continental - Fluvial \| Continental - Lacustrine \| Transitional - Deltaic \| Transitional - Tidal \| Transitional - Lagoonal \| Transitional - Beach \| Transitional - Lake \| Marine - Shallow \| Marine - Deep \| Marine - Reef \| Other - Evaporite \| Other - Glacial \| Other - Volcanic \| other]|
|**Example**|Continental - Alluvial|


## diss_carb_dioxide
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|dissolved carbon dioxide|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000436|
|**Definition**|Concentration of dissolved carbon dioxide in the sample or liquid portion of the sample|


## diss_inorg_carb
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|dissolved inorganic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2059 micromole per kilogram|
|**Preferred unit**|microgram per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000434|
|**Definition**|Dissolved inorganic carbon concentration in the sample, typically measured after filtering the sample using a 0.45 micrometer filter|


## diss_inorg_phosp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|dissolved inorganic phosphorus|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|56.5 micromole per liter|
|**Preferred unit**|microgram per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000106|
|**Definition**|Concentration of dissolved inorganic phosphorus in the sample|


## diss_iron
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|dissolved iron|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## diss_org_carb
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|dissolved organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|197 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000433|
|**Definition**|Concentration of dissolved organic carbon in the sample, liquid portion of the sample, or aqueous phase of the fluid|


## diss_oxygen_fluid
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|dissolved oxygen in fluids|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## elev
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## ethylbenzene
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|ethylbenzene|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## field
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|field name|
|**Requirement**|Condition Specific|
|**Value syntax**|{text}|


## hc_produced
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|hydrocarbon type produced|
|**Requirement**|Mandatory|
|**Value syntax**|[Oil \| Gas-Condensate \| Gas \| Bitumen \| Coalbed Methane \| other]|
|**Example**|Gas|


## hcr
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|hydrocarbon resource type|
|**Requirement**|Mandatory|
|**Value syntax**|[Oil Reservoir \| Gas Reservoir \| Oil Sand \| Coalbed \| Shale \| Tight Oil Reservoir \| Tight Gas Reservoir \| other]|
|**Example**|Oil Sand|


## hcr_fw_salinity
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|formation water salinity|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## hcr_geol_age
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|hydrocarbon resource geological age|
|**Requirement**|Condition Specific|
|**Value syntax**|[Archean \| Cambrian \| Carboniferous \| Cenozoic \| Cretaceous \| Devonian \| Jurassic \| Mesozoic \| Neogene \| Ordovician \| Paleogene \| Paleozoic \| Permian \| Precambrian \| Proterozoic \| Silurian \| Triassic \| other]|
|**Example**|Silurian|


## hcr_pressure
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|hydrocarbon resource original pressure|
|**Requirement**|Optional|
|**Value syntax**|{float} - {float} {unit}|


## hcr_temp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|hydrocarbon resource original temperature|
|**Requirement**|Mandatory|
|**Value syntax**|{float} - {float} {unit}|
|**Example**|150-295 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000393|
|**Definition**|Original temperature of the hydrocarbon resource|


## lithology
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|lithology|
|**Requirement**|Condition Specific|
|**Value syntax**|[Basement \| Chalk \| Chert \| Coal \| Conglomerate \| Diatomite \| Dolomite \| Limestone \| Sandstone \| Shale \| Siltstone \| Volcanic \| other]|
|**Example**|Volcanic|


## magnesium
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|magnesium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|52.8 micromole per kilogram|
|**Preferred unit**|mole per liter, milligram per liter, parts per million, micromole per kilogram|
|**URL**|https://w3id.org/mixs/terms/0000431|
|**Definition**|Concentration of magnesium in the sample|


## misc_param
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## nitrate
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|nitrate|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|65 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000425|
|**Definition**|Concentration of nitrate in the sample|


## nitrite
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|nitrite|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|0.5 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000426|
|**Definition**|Concentration of nitrite in the sample|


## org_count_qpcr_info
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|organism count qPCR information|
|**Requirement**|Optional|
|**Value syntax**|{text};FWD:{dna};REV:{dna};initial denaturation:degrees_minutes;denaturation:degrees_minutes;annealing:degrees_minutes;elongation:degrees_minutes;final elongation:degrees_minutes; total cycles|


## organism_count
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Condition Specific|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## owc_tvdss
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|oil water contact depth|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## oxy_stat_samp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## permeability
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|permeability|
|**Requirement**|Optional|
|**Value syntax**|{integer} - {integer} {unit}|


## ph
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|pH|
|**Requirement**|Condition Specific|
|**Value syntax**|{float}|
|**Example**|7.2|


## porosity
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|porosity|
|**Requirement**|Optional|
|**Value syntax**|{float} - {float} {unit}|


## potassium
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|potassium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|463 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000430|
|**Definition**|Concentration of potassium in the sample|


## pour_point
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|pour point|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## pressure
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|pressure|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 atmosphere|
|**Preferred unit**|atmosphere|
|**URL**|https://w3id.org/mixs/terms/0000412|
|**Definition**|Pressure to which the sample is subject to, in atmospheres|


## reservoir
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|reservoir name|
|**Requirement**|Condition Specific|
|**Value syntax**|{text}|


## resins_pc
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|resins wt%|
|**Requirement**|Condition Specific|
|**Value syntax**|{text};{float} {unit}|


## salinity
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## samp_md
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample measured depth|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};[GL \| DF \| RT \| KB \| MSL \| other]|
|**Example**|1534 meter;MSL|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000413|
|**Definition**|In non deviated well, measured depth is equal to the true vertical depth, TVD (TVD=TVDSS plus the reference or datum it refers to). In deviated wells, the MD is the length of trajectory of the borehole measured from the same reference or datum. Common datums used are ground level (GL), drilling rig floor (DF), rotary table (RT), kelly bushing (KB) and mean sea level (MSL). If "other" is specified, please propose entry in "additional info" field|


## samp_store_dur
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_subtype
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample subtype|
|**Requirement**|Condition Specific|
|**Value syntax**|[oil phase \| water phase \| biofilm \| not applicable \| other]|
|**Example**|biofilm|


## samp_transport_cond
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample transport conditions|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{float} {unit}|
|**Example**|5 days;-20 degree Celsius|
|**Preferred unit**|days;degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000410|
|**Definition**|Sample transport duration (in days or hrs) and temperature the sample was exposed to (e.g. 5.5 days; 20 ¬∞C)|


## samp_tvdss
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample true vertical depth subsea|
|**Requirement**|Condition Specific|
|**Value syntax**|{float}-{float} {unit}|


## samp_type
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample type|
|**Requirement**|Mandatory|
|**Value syntax**|[core \| rock trimmings \| drill cuttings \| piping section \| coupon \| pigging debris \| solid deposit \| produced fluid \| produced water \| injected water \| water from treatment plant \| fresh water \| sea water \| drilling fluid \| procedural blank \| positive control \| negative control \| other]|
|**Example**|solid deposit|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## samp_well_name
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sample well name|
|**Requirement**|Condition Specific|
|**Value syntax**|{text}|


## saturates_pc
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|saturates wt%|
|**Requirement**|Condition Specific|
|**Value syntax**|{text};{float} {unit}|


## sodium
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sodium|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10.5 milligram per liter|
|**Preferred unit**|milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000428|
|**Definition**|Sodium concentration in the sample|


## sr_dep_env
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|source rock depositional environment|
|**Requirement**|Optional|
|**Value syntax**|[Lacustine \| Fluvioldeltaic \| Fluviomarine \| Marine \| other]|
|**Example**|Marine|


## sr_geol_age
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|source rock geological age|
|**Requirement**|Optional|
|**Value syntax**|[Archean \| Cambrian \| Carboniferous \| Cenozoic \| Cretaceous \| Devonian \| Jurassic \| Mesozoic \| Neogene \| Ordovician \| Paleogene \| Paleozoic \| Permian \| Precambrian \| Proterozoic \| Silurian \| Triassic \| other]|
|**Example**|Silurian|


## sr_kerog_type
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|source rock kerogen type|
|**Requirement**|Optional|
|**Value syntax**|[Type I \| Type II \| Type III \| Type IV \| other]|
|**Example**|Type IV|


## sr_lithology
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|source rock lithology|
|**Requirement**|Optional|
|**Value syntax**|[Clastic \| Carbonate \| Coal \| Biosilicieous \| other]|
|**Example**|Coal|


## sulfate
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sulfate|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|5 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000423|
|**Definition**|Concentration of sulfate in the sample|


## sulfate_fw
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sulfate in formation water|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|


## sulfide
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|sulfide|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|2 micromole per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000424|
|**Definition**|Concentration of sulfide in the sample|


## suspend_solids
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|suspended solids|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|


## tan
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|total acid number|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## temp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## toluene
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|toluene|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## tot_iron
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|total iron|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## tot_nitro
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|total nitrogen concentration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|50 micromole per liter|
|**Preferred unit**|microgram per liter, micromole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000102|
|**Definition**|Total nitrogen concentration of water samples, calculated by: total nitrogen = total dissolved nitrogen + particulate nitrogen. Can also be measured without filtering, reported as nitrogen|


## tot_phosp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|total phosphorus|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|0.03 milligram per liter|
|**Preferred unit**|micromole per liter, milligram per liter, parts per million|
|**URL**|https://w3id.org/mixs/terms/0000117|
|**Definition**|Total phosphorus concentration in the sample, calculated by: total phosphorus = total dissolved phosphorus + particulate phosphorus|


## tot_sulfur
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|total sulfur|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## tvdss_of_hcr_press
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|depth (TVDSS) of hydrocarbon resource pressure|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## tvdss_of_hcr_temp
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|depth (TVDSS) of hydrocarbon resource temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## vfa
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|volatile fatty acids|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|


## vfa_fw
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|vfa in formation water|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|


## viscosity
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|viscosity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{float} {unit}|


## win
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|well identification number|
|**Requirement**|Condition Specific|
|**Value syntax**|{text}|


## xylene
|||
|---|---|
|**Package**|hydrocarbon resources-cores|
|**Item (rdfs:label)**|xylene|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
