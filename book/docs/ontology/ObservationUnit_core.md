# ObservationUnit core

## ObservationUnitIdentifier
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Observation unit identifier|
|**Requirement**|Mandatory|
|**Value syntax**|{id}{5,25}|
|**Example**|S1005TX|
|**URL**|http://schema.org/identifier|
|**Definition**|Identifier corresponding to the entity that is being observed|


## ObservationUnitName
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Observation unit name|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{10,}|
|**Example**|ObservationX Name|
|**URL**|http://schema.org/name|
|**Definition**|Name of the entity being observed|


## ObservationUnitDescription
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Observation unit description|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{25,}|
|**Example**|ObservationX description|
|**URL**|http://schema.org/description|
|**Definition**|Description of the entity being observed|
