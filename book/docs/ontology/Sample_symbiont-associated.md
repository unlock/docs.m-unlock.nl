# Sample symbiont-associated

## lat_lon
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|geographic location (latitude and longitude)|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {float}|
|**Example**|50.586825 6.408977|


## geo_loc_name
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|geographic location (country and/or sea,region)|
|**Requirement**|Mandatory|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Maryland, Bethesda|


## collection_date
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|collection date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00; 2018-05-11|


## env_broad_scale
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|broad-scale environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|forest biome [ENVO:01000174]|


## env_local_scale
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|local environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|litter layer [ENVO:01000338]|


## env_medium
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|environmental medium|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|intestine environment [ENVO:2100002]|


## alt
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|altitude|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000094|
|**Definition**|Altitude is a term used to identify heights of objects such as airplanes, space shuttles, rockets, atmospheric balloons and heights of places such as atmospheric layers and clouds. It is used to measure the height of an object which is above the earth's surface. In this context, the altitude measurement is the vertical distance between the earth's surface above sea level and the sampled position in the air.|


## depth
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|depth|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. for sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## elev
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit.|


## host_subject_id
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host subject id|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|P14|


## host_common_name
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host common name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|trematode|


## host_taxid
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host taxid|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|395013|


## source_mat_id
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|source material identifiers|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|MPI012345|


## host_dependence
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host dependence|
|**Requirement**|Mandatory|
|**Value syntax**|[facultative \| obligate]|
|**Example**|obligate|


## type_of_symbiosis
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|type of symbiosis|
|**Requirement**|Condition Specific|
|**Value syntax**|[commensalistic \| mutualistic \| parasitic]|
|**Example**|parasitic|


## sym_life_cycle_type
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|symbiotic host organism life cycle type|
|**Requirement**|Mandatory|
|**Value syntax**|[complex life cycle  \|  simple life cycle]|
|**Example**|complex life cycle|


## host_life_stage
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host life stage|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|redia|


## host_age
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## host_sex
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host sex|
|**Requirement**|Optional|
|**Value syntax**|[female \| hermaphrodite \| male \| neuter]|


## mode_transmission
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|mode of transmission|
|**Requirement**|Condition Specific|
|**Value syntax**|[horizontal:castrator \| horizontal:directly transmitted \| horizontal:micropredator \| horizontal:parasitoid \| horizontal:trophically transmitted \| horizontal:vector transmitted \| vertical]|
|**Example**|horizontal:castrator|


## route_transmission
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|route of transmission|
|**Requirement**|Optional|
|**Value syntax**|[environmental:faecal-oral \| transplacental \| vector-borne:vector penetration]|


## host_body_habitat
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host body habitat|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|anterior end of a tapeworm|


## host_body_site
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host body site|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|scolex [UBERON:0015119]|


## host_body_product
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host body product|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|Portion of mucus [fma66938]|


## host_tot_mass
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host total mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## host_height
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host height|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## host_length
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host length|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## host_growth_cond
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host growth conditions|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|


## host_substrate
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host substrate|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_family_relationship
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host family relationship|
|**Requirement**|Optional|
|**Value syntax**|{text};{text}|
|**Example**|clone;P15|


## host_infra_specific_name
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host infra-specific name|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_infra_specific_rank
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host infra-specific rank|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_genotype
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host genotype|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_phenotype
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host phenotype|
|**Requirement**|Optional|
|**Value syntax**|{term}|
|**Example**|soldier|


## host_dry_mass
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host dry mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## host_color
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host color|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_shape
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host shape|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## gravidity
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|gravidity|
|**Requirement**|Optional|
|**Value syntax**|{boolean};{timestamp}|


## host_number
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host number individual|
|**Requirement**|Optional|
|**Value syntax**|{float} m|
|**Example**|3|


## host_symbiont
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|observed host symbionts|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Paragordius varius|


## host_specificity
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host specificity|
|**Requirement**|Condition Specific|
|**Value syntax**|[family-specific \| generalist \|  \| genus-specific \| species-specific]|
|**Example**|species-specific|


## symbiont_host_role
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiont role|
|**Requirement**|Condition Specific|
|**Value syntax**|[accidental \| dead-end \| definitive \| intermediate \| paratenic \| reservoir \| single host]|
|**Example**|intermediate|


## host_cellular_loc
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host cellular location|
|**Requirement**|Condition Specific|
|**Value syntax**|[intracellular \| extracellular \| not determined]|
|**Example**|extracellular|


## association_duration
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|duration of association with the host|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## host_of_host_coinf
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|observed coinfecting organisms in host of host|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Maritrema novaezealandense|


## host_of_host_name
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host common name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|snail|


## host_of_host_env_loc
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host local environmental context|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|small intestine[uberon:0002108]|


## host_of_host_env_med
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host environemental medium|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|feces[uberon:0001988]|


## host_of_host_taxid
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host taxon id|
|**Requirement**|Optional|
|**Value syntax**|{integer}|
|**Example**|145637|


## host_of_host_sub_id
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host subject id|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|H3|


## host_of_host_disease
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host disease status|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|rabies [DOID:11260]|


## host_of_host_fam_rel
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host family relationship|
|**Requirement**|Optional|
|**Value syntax**|{text};{text}|


## host_of_host_infname
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host infra-specific name|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_of_host_infrank
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host infra-specific rank|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_of_host_geno
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host genotype|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## host_of_host_pheno
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host phenotype|
|**Requirement**|Optional|
|**Value syntax**|{term}|


## host_of_host_gravid
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host gravidity|
|**Requirement**|Optional|
|**Value syntax**|{boolean};{timestamp}|


## host_of_host_totmass
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|host of the symbiotic host total mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## chem_administration
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{term}; {timestamp}|


## perturbation
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|


## samp_salinity
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|sample salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 milligram per liter|
|**Preferred unit**|milligram per liter, practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000109|
|**Definition**|Salinity is the total concentration of all dissolved salts in a liquid or solid (in the form of an extract obtained by centrifugation) sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## oxy_stat_samp
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic]|
|**Example**|aerobic|


## temp
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling.|


## organism_count
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of organism per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr).|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## samp_store_temp
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## samp_store_dur
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_sol
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|sample storage solution|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|5% ethanol|


## misc_param
|||
|---|---|
|**Package**|symbiont-associated|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
