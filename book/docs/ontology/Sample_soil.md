# Sample soil

## agrochem_addition
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/agrochemical additions|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{timestamp}|
|**Example**|roundup;5 milligram per liter;2018-06-21|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000639|
|**Definition**|Addition of fertilizers, pesticides, etc. - amount and time of applications|


## al_sat
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|extreme_unusual_properties/Al saturation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## al_sat_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|extreme_unusual_properties/Al saturation method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## annual_precpt
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|mean annual precipitation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## annual_temp
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|mean annual temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|12.5 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000642|
|**Definition**|Mean annual temperature|


## crop_rotation
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/crop rotation|
|**Requirement**|Optional|
|**Value syntax**|{boolean};{Rn/start_time/end_time/duration}|
|**Example**|yes;R2/2017-01-01/2018-12-31/P6M|


## cur_land_use
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|current land use|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|conifers|


## cur_vegetation
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|current vegetation|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## cur_vegetation_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|current vegetation method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## depth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|depth|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. For sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## drainage_class
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|drainage classification|
|**Requirement**|Optional|
|**Value syntax**|[very poorly \| poorly \| somewhat poorly \| moderately well \| well \| excessively drained]|
|**Example**|well|


## elev
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## extreme_event
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/extreme events|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|


## fao_class
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil_taxonomic/FAO classification|
|**Requirement**|Optional|
|**Value syntax**|[Acrisols \| Andosols \| Arenosols \| Cambisols \| Chernozems \| Ferralsols \| Fluvisols \| Gleysols \| Greyzems \| Gypsisols \| Histosols \| Kastanozems \| Lithosols \| Luvisols \| Nitosols \| Phaeozems \| Planosols \| Podzols \| Podzoluvisols \| Rankers \| Regosols \| Rendzinas \| Solonchaks \| Solonetz \| Vertisols \| Yermosols]|
|**Example**|Luvisols|


## fire
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/fire|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|


## flooding
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/flooding|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|


## heavy_metals
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|extreme_unusual_properties/heavy metals|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|mercury;0.09 micrograms per gram|
|**Preferred unit**|microgram per gram|
|**URL**|https://w3id.org/mixs/terms/0000652|
|**Definition**|Heavy metals present in the sequenced sample and their concentrations. For multiple heavy metals and concentrations, add multiple copies of this field.|


## heavy_metals_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|extreme_unusual_properties/heavy metals method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## horizon_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|horizon method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## link_addit_analys
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|links to additional analysis|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## link_class_info
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|link to classification information|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## link_climate_info
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|link to climate information|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## local_class
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil_taxonomic/local classification|
|**Requirement**|Optional|
|**Value syntax**|{text}|


## local_class_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil_taxonomic/local classification method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## micro_biomass_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|microbial biomass method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## microbial_biomass
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|microbial biomass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## misc_param
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## ph
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.2|


## ph_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|pH method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## pool_dna_extracts
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|pooling of DNA extracts (if done)|
|**Requirement**|Optional|
|**Value syntax**|{boolean};{integer}|
|**Example**|yes;5|


## prev_land_use_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/previous land use method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## previous_land_use
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/previous land use|
|**Requirement**|Optional|
|**Value syntax**|{text};{timestamp}|


## profile_position
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|profile position|
|**Requirement**|Optional|
|**Value syntax**|[summit \| shoulder \| backslope \| footslope \| toeslope]|
|**Example**|summit|


## salinity_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|salinity method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## season_precpt
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|mean seasonal precipitation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## season_temp
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|mean seasonal temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|18 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000643|
|**Definition**|Mean seasonal temperature|


## sieving
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|composite design/sieving|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {float} {unit}};{float} {unit}|


## slope_aspect
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|slope aspect|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## slope_gradient
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|slope gradient|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## soil_horizon
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil horizon|
|**Requirement**|Optional|
|**Value syntax**|[O horizon \| A horizon \| E horizon \| B horizon \| C horizon \| R layer \| Permafrost]|
|**Example**|A horizon|


## soil_text_measure
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil texture measurement|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## soil_texture_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil texture method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## soil_type
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil type|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|plinthosol [ENVO:00002250]|


## soil_type_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|soil type method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## store_cond
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|storage conditions|
|**Requirement**|Optional|
|**Value syntax**|{text};{duration}|
|**Example**|-20 degree Celsius freezer;P2Y10D|


## tillage
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|history/tillage|
|**Requirement**|Optional|
|**Value syntax**|[drill \| cutting disc \| ridge till \| strip tillage \| zonal tillage \| chisel \| tined \| mouldboard \| disc plough]|
|**Example**|chisel|


## tot_nitro_cont_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|total nitrogen content method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## tot_nitro_content
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|total nitrogen content|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## tot_org_c_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|total organic carbon method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## tot_org_carb
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|total organic carbon|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|


## water_cont_soil_meth
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|water content method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|


## water_content
|||
|---|---|
|**Package**|soil|
|**Item (rdfs:label)**|water content|
|**Requirement**|Optional|
|**Value syntax**|{float}|
