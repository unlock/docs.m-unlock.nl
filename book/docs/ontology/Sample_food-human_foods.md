# Sample food-human foods

## lat_lon
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|geographic location (latitude and longitude)|
|**Requirement**|Mandatory|
|**Value syntax**|{float} {float}|
|**Example**|50.586825 6.408977|


## geo_loc_name
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|geographic location (country and/or sea,region)|
|**Requirement**|Mandatory|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Maryland, Bethesda|


## collection_date
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|collection date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## env_broad_scale
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|broad-scale environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|oceanic epipelagic zone biome [ENVO:01000033] for annotating a water sample from the photic zone in middle of the Atlantic Ocean|


## env_local_scale
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|local environmental context|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|litter layer [ENVO:01000338]; Annotating a pooled sample taken from various vegetation layers in a forest consider: canopy [ENVO:00000047] \| herb and fern layer [ENVO:01000337] \| litter layer [ENVO:01000338] \| understory [01000335] \| shrub layer [ENVO:01000336].|


## env_medium
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|environmental medium|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|soil [ENVO:00001998]; Annotating a fish swimming in the upper 100 m of the Atlantic Ocean, consider: ocean water [ENVO:00002151]. Example: Annotating a duck on a pond consider: pond water [ENVO:00002228] \| air [ENVO_00002005]|


## seq_meth
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sequencing method|
|**Requirement**|Mandatory|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|454 Genome Sequencer FLX [OBI:0000702]|


## samp_size
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|amount or size of sample collected|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|5 liters|
|**Preferred unit**|liter, gram, cm^2|
|**URL**|https://w3id.org/mixs/terms/0000001|
|**Definition**|The total amount or size (volume (ml), mass (g) or area (m2) ) of sample collected.|


## samp_collect_device
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample collection device|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|swab, biopsy, niskin bottle, push core, drag swab [GENEPIO:0002713]|


## experimental_factor
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|experimental factor|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {text}|
|**Example**|time series design [EFO:0001779]|


## nucl_acid_ext
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|nucleic acid extraction|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|https://mobio.com/media/wysiwyg/pdfs/protocols/12888.pdf|


## organism_count
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## spikein_count
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike-in organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| other]|
|**Example**|total prokaryotes;3.5e7 colony forming units per milliliter;qPCR|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0001335|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided (example: total prokaryotes; 3.5e7 cells per ml; qPCR).|


## samp_stor_temp
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius.|


## samp_stor_dur
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|milliliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## pool_dna_extracts
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|pooling of DNA extracts (if done)|
|**Requirement**|Optional|
|**Value syntax**|{boolean},{integer}|
|**Example**|yes, 5|


## temp
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling.|


## samp_stor_loc
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|freezer 5|


## genetic_mod
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|genetic modification|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL}|
|**Example**|aox1A transgenic|


## perturbation
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## coll_site_geo_feat
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|collection site geographic feature|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|grocery store [ENVO:01000984]|


## food_product_type
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food product type|
|**Requirement**|Mandatory|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|shrimp (peeled, deep-frozen) [FOODON:03317171]|


## IFSAC_category
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|Interagency Food Safety Analytics Collaboration (IFSAC) category|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Plants:Produce:Vegetables:Herbs:Dried Herbs|


## ferm_chem_add
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation chemical additives|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|salt|


## ferm_chem_add_perc
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation chemical additives percentage|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} percentage|
|**Example**|0.01|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0001186|
|**Definition**|The amount of chemical added to the fermentation process.|


## ferm_headspace_oxy
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation headspace oxygen|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} percentage|
|**Example**|0.05|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0001187|
|**Definition**|The amount of headspace oxygen in a fermentation vessel.|


## ferm_medium
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation medium|
|**Requirement**|Condition Specific|
|**Value syntax**|{text}|
|**Example**|molasses|


## ferm_pH
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation pH|
|**Requirement**|Condition Specific|
|**Value syntax**|{float}|
|**Example**|4.5|


## ferm_rel_humidity
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation relative humidity|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|95% RH|
|**Preferred unit**|percentage|
|**URL**|https://w3id.org/mixs/terms/0001190|
|**Definition**|The relative humidity of the fermented food fermentation process.|


## ferm_temp
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation temperature|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|22 degrees Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001191|
|**Definition**|The temperature of the fermented food fermentation process.|


## ferm_time
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation time|
|**Requirement**|Condition Specific|
|**Value syntax**|{duration}|
|**Example**|P10D|
|**Preferred unit**|days|
|**URL**|https://w3id.org/mixs/terms/0001192|
|**Definition**|The time duration of the fermented food fermentation process.|


## ferm_vessel
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|fermentation vessel|
|**Requirement**|Condition Specific|
|**Value syntax**|{text}|
|**Example**|steel drum|


## bacterial_density
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|bacteria density|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 colony forming units per gram dry weight|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0001194|
|**Definition**|Number of bacteria in sample, as defined by bacteria density (http://purl.obolibrary.org/obo/GENEPIO_0000043).|


## cons_food_stor_dur
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food stored by consumer (storage duration)|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P5D|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001195|
|**Definition**|The storage duration of the food commodity by the consumer, prior to onset of illness or sample collection. Indicate the timepoint written in ISO 8601 format.|


## cons_food_stor_temp
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food stored by consumer (storage temperature)|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit} \| {text}|
|**Example**|4 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001196|
|**Definition**|Temperature at which food commodity was stored by the consumer, prior to onset of illness or sample collection.|


## cons_purch_date
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|purchase date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|43799|


## cons_qty_purchased
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|quantity purchased|
|**Requirement**|Optional|
|**Value syntax**|{integer} {unit}|
|**Example**|5 cans|


## cult_isol_date
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|culture isolation date|
|**Requirement**|Optional|
|**Value syntax**|{timestamp}|
|**Example**|2018-05-11T10:00:00+01:00|


## cult_result
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|culture result|
|**Requirement**|Optional|
|**Value syntax**|[absent \| active \| inactive \| negative \| no \| present \| positive \| yes]|
|**Example**|positive|


## cult_result_org
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|culture result organism|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## cult_target
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|culture target microbial analyte|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502]|


## dietary_claim_use
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|dietary claim or use|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|No preservatives [FOODON:03510113]|


## enrichment_protocol
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|enrichment protocol|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|BAM Chapter 4: Enumeration of Escherichia coli and the Coliform Bacteria|


## food_additive
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food additive|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|xanthan gum [FOODON:03413321]|


## food_allergen_label
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food allergen labeling|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food allergen labelling about crustaceans and products thereof [FOODON:03510215]|


## food_contact_surf
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food contact surface|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|cellulose acetate [FOODON:03500034]|


## food_contain_wrap
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food container or wrapping|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Plastic shrink-pack [FOODON:03490137]|


## food_cooking_proc
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food cooking process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food blanching [FOODON:03470175]|


## food_dis_point
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food distribution point geographic location|
|**Requirement**|Optional|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Delmarva Peninsula or USA: Georgia, Atlanta [GAZ:00004445]|


## food_ingredient
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food ingredient|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|bean (whole) [FOODON:00002753]|


## food_name_status
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food product name legal status|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|protected geographic indication [FOODON:03530256]|


## food_origin
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food product origin geographic location|
|**Requirement**|Optional|
|**Value syntax**|{term}: {term}, {text}|
|**Example**|USA: Delmarva, Peninsula|


## food_pack_capacity
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food package capacity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|454 grams|
|**Preferred unit**|grams|
|**URL**|https://w3id.org/mixs/terms/0001208|
|**Definition**|The maximum number of product units within a package|


## food_pack_integrity
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food packing medium integrity|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food packing medium compromised [FOODON:00002517]|


## food_pack_medium
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food packing medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|vacuum-packed[FOODON:03480027]|


## food_preserv_proc
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food preservation process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|food slow freezing [FOODON:03470128]|


## food_prior_contact
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|material of contact prior to food packaging|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|processed in stainless steel container [FOODON:03530081]|


## food_prod
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food production system characteristics|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|organic plant cultivation [FOODON:03530253]|


## food_prod_synonym
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food product synonym|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|pinot gris|


## food_product_qual
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food product by quality|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|raw [FOODON:03311126]|


## food_quality_date
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food quality date|
|**Requirement**|Optional|
|**Value syntax**|[best by \| best if used by \| freeze by \|  \| use by]; date|
|**Example**|best by 2020-05-24|


## food_source
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food source|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|giant tiger prawn [FOODON:03412612]|


## food_trace_list
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food traceability list category|
|**Requirement**|Optional|
|**Value syntax**|[cheeses-other than hard cheeses \| cucumbers \| crustaceans \| finfish-including smoked finfish \| fruits and vegetables-fresh cut \| herbs-fresh \| leafy greens-including fresh cut leafy greens \| melons \| mollusks-bivalves \| nut butter \| peppers \| ready to eat deli salads \| sprouts \| tomatoes \| tropical tree fruits \| shell eggs]|
|**Example**|tropical tree fruits|


## food_trav_mode
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food shipping transportation method|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|train travel [GENEPIO:0001060]|


## food_trav_vehic
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|Food shipping transportation vehicle|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|aircraft [ENVO:01001488] \| car [ENVO:01000605]|


## food_treat_proc
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|food treatment process|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|gluten removal process [FOODON:03460750]|


## HACCP_term
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|Hazard Analysis Critical Control Points (HACCP) guide food safety term|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|tetrodotoxic poisoning [FOODON:03530249]|


## intended_consumer
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|intended consumer|
|**Requirement**|Optional|
|**Value syntax**|{integer} \| {termLabel} {[termID]}|
|**Example**|9606 o rsenior as food consumer [FOODON:03510254]|


## library_prep_kit
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|library preparation kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Illumina DNA Prep|


## lot_number
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|lot number|
|**Requirement**|Optional|
|**Value syntax**|{integer}, {text}|
|**Example**|1239ABC01A, split Cornish hens|


## microb_cult_med
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbiological culture medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|brain heart infusion agar [MICRO:0000566]|


## microb_start
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbial starter|
|**Requirement**|Optional|
|**Value syntax**|{term label} {[termID]} \| {text}|
|**Example**|starter cultures [FOODON:03544454]|


## microb_start_count
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbial starter organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[ATP \| MPN \| qPCR \| spread plate \| other]|
|**Example**|total prokaryotes;3.5e9 colony forming units per milliliter;spread plate|
|**Preferred unit**|colony forming units per milliliter; colony forming units per gram of dry weight|
|**URL**|https://w3id.org/mixs/terms/0001218|
|**Definition**|Total cell count of starter culture per gram, volume or area of sample and the method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) should also be provided. (example : total prokaryotes; 3.5e7 cells per ml; qPCR).|


## microb_start_inoc
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbial starter inoculation|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|100 milligrams|
|**Preferred unit**|milligram or gram|
|**URL**|https://w3id.org/mixs/terms/0001219|
|**Definition**|The amount of starter culture used to inoculate a new batch.|


## microb_start_prep
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbial starter preparation|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|liquid starter culture, propagated 3 cycles in milk prior to inoculation|


## microb_start_source
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbial starter source|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|backslopped, GetCulture|


## microb_start_taxID
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|microbial starter NCBI taxonomy ID|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Lactobacillus rhamnosus [NCIT:C123495]|


## nucl_acid_ext_kit
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|nucleic acid extraction kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Qiagen PowerSoil Kit|


## num_samp_collect
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|number of samples collected|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|116 samples|


## part_plant_animal
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|part of plant or animal|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|chuck [FOODON:03530021]|


## repository_name
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|repository name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|FDA CFSAN Microbiology Laboratories|


## sample_collec_method
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample collection method|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|environmental swab sampling|


## samp_pooling
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample pooling|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|5uL of extracted genomic DNA from 5 leaves were pooled|


## samp_rep_biol
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|biological sample replicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|6 replicates|


## samp_rep_tech
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|technical sample replicate|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 replicates|


## samp_source_mat_cat
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample source material category|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|environmental (swab or sampling) [GENEPIO:0001732]|


## samp_stor_device
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample storage device|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Whirl Pak sampling bag [GENEPIO:0002122]|


## samp_stor_media
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample storage media|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|peptone water medium [MICRO:0000548]|


## samp_transport_cont
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample transport container|
|**Requirement**|Optional|
|**Value syntax**|[bottle \| cooler \| glass vial \| plastic vial \|  \| vendor supplied container]|
|**Example**|cooler|


## samp_transport_dur
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample transport duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P10D|
|**Preferred unit**|days|
|**URL**|https://w3id.org/mixs/terms/0001231|
|**Definition**|The duration of time from when the sample was collected until processed. Indicate the duration for which the sample was stored written in ISO 8601 format.|


## samp_transport_temp
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sample transport temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit} {text}|
|**Example**|4 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001232|
|**Definition**|Temperature at which sample was transported, e.g. -20 or 4 degree Celsius.|


## samp_purpose
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|purpose of sampling|
|**Requirement**|Optional|
|**Value syntax**|[active surveillance in response to an outbreak \| active surveillance not initiated by an outbreak \| clinical trial \| cluster investigation \| environmental assessment \| farm sample \| field trial \| for cause \| industry internal investigation \| market sample \| passive surveillance \| population based studies \| research \| research and development] or {text}|
|**Example**|field trial|


## sequencing_kit
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sequencing kit|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NextSeq 500/550 High Output Kit v2.5 (75 Cycles)|


## sequencing_location
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|sequencing location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|University of Maryland Genomics Resource Center|


## serovar_or_serotype
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|serovar or serotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Escherichia coli strain O157:H7 [NCIT:C86883]|


## spikein_AMR
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|antimicrobial phenotype of spike-in bacteria|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{termLabel} {[termID]}|
|**Example**|wild type [ARO:3004432]|


## spikein_antibiotic
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike-in with antibiotics|
|**Requirement**|Optional|
|**Value syntax**|{text} {integer}|
|**Example**|Tetracycline at 5 mg/ml|


## spikein_growth_med
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike-in growth medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|LB broth [MCO:0000036]|


## spikein_metal
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike-in with heavy metals|
|**Requirement**|Optional|
|**Value syntax**|{text} {integer}|
|**Example**|Cd at 20 ppm|


## spikein_org
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike in organism|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Listeria monocytogenes [NCIT:C86502] \| 28901|


## spikein_serovar
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike-in bacterial serovar or serotype|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|Escherichia coli strain O157:H7 [NCIT:C86883] \| 83334|


## spikein_strain
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|spike-in microbial strain|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} \| {integer}|
|**Example**|169963|


## study_design
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|study design|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|in vitro design [OBI:0001285]|


## study_inc_dur
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|study incubation duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001237|
|**Definition**|Sample incubation duration if unpublished or unvalidated method is used. Indicate the timepoint written in ISO 8601 format.|


## study_inc_temp
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|study incubation temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|37 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0001238|
|**Definition**|Sample incubation temperature if unpublished or unvalidated method is used.|


## study_timecourse
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|time-course duration|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2 days post inoculation|
|**Preferred unit**|dpi|
|**URL**|https://w3id.org/mixs/terms/0001239|
|**Definition**|For time-course research studies involving samples of the food commodity, indicate the total duration of the time-course study.|


## study_tmnt
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|study treatment|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Factor A \| spike-in \| levels high, medium, low|


## timepoint
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|timepoint|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|P24H|
|**Preferred unit**|hours or days|
|**URL**|https://w3id.org/mixs/terms/0001173|
|**Definition**|Time point at which a sample or observation is made or taken from a biomaterial as measured from some reference point. Indicate the timepoint written in ISO 8601 format.|


## misc_param
|||
|---|---|
|**Package**|food-human foods|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|
