# Sample plant-associated

## air_temp_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|air temperature regimen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|25 degree Celsius;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000551|
|**Definition**|Information about treatment involving an exposure to varying temperatures; should include the temperature, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include different temperature regimens|


## ances_data
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|ancestral data|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|A/3*B|


## antibiotic_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|antibiotic regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|penicillin;5 milligram;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|milligram|
|**URL**|https://w3id.org/mixs/terms/0000553|
|**Definition**|Information about treatment involving antibiotic administration; should include the name of antibiotic, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple antibiotic regimens|


## biol_stat
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|biological status|
|**Requirement**|Optional|
|**Value syntax**|[wild \| natural \| semi-natural \| inbred line \| breeder's line \| hybrid \| clonal selection \| mutant]|
|**Example**|natural|


## biotic_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|biotic regimen|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|sample inoculated with Rhizobium spp. Culture|


## chem_administration
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|chemical administration|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}; {timestamp}|
|**Example**|agar [CHEBI:2509];2018-05-11T20:00Z|


## chem_mutagen
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|chemical mutagen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|nitrous acid;0.5 milligram per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000555|
|**Definition**|Treatment involving use of mutagens; should include the name of mutagen, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple mutagen regimens|


## climate_environment
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|climate environment|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|tropical climate;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|


## cult_root_med
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|culture rooting medium|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {PMID} \| {DOI} \| {URL}|
|**Example**|http://himedialabs.com/TD/PT158.pdf|


## depth
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|depth|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|10 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000018|
|**Definition**|The vertical distance below local surface, e.g. For sediment or soil samples depth is measured from sediment or soil surface, respectively. Depth can be reported as an interval for subsurface samples.|


## elev
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|elevation|
|**Requirement**|Condition Specific|
|**Value syntax**|{float} {unit}|
|**Example**|100 meter|
|**Preferred unit**|meter|
|**URL**|https://w3id.org/mixs/terms/0000093|
|**Definition**|Elevation of the sampling site is its height above a fixed reference point, most commonly the mean sea level. Elevation is mainly used when referring to points on the earth's surface, while altitude is used for points above the surface, such as an aircraft in flight or a spacecraft in orbit|


## fertilizer_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|fertilizer regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|urea;0.6 milligram per liter;R2/2018-05-11:T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000556|
|**Definition**|Information about treatment involving the use of fertilizers; should include the name of fertilizer, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple fertilizer regimens|


## fungicide_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|fungicide regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|bifonazole;1 mole per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000557|
|**Definition**|Information about treatment involving use of fungicides; should include the name of fungicide, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple fungicide regimens|


## gaseous_environment
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|gaseous environment|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|nitric oxide;0.5 micromole per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|micromole per liter|
|**URL**|https://w3id.org/mixs/terms/0000558|
|**Definition**|Use of conditions with differing gaseous environments; should include the name of gaseous compound, amount administered, treatment duration, interval and total experimental duration; can include multiple gaseous environment regimens|


## genetic_mod
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|genetic modification|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|aox1A transgenic|


## gravity
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|gravity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|12 g;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|meter per square second, g|
|**URL**|https://w3id.org/mixs/terms/0000559|
|**Definition**|Information about treatment involving use of gravity factor to study various types of responses in presence, absence or modified levels of gravity; treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple treatments|


## growth_facil
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|growth facility|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|Growth chamber [CO_715:0000189]|


## growth_habit
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|growth habit|
|**Requirement**|Optional|
|**Value syntax**|[erect \| semi-erect \| spreading \| prostrate]|
|**Example**|spreading|


## growth_hormone_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|growth hormone regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|abscisic acid;0.5 milligram per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000560|
|**Definition**|Information about treatment involving use of growth hormones; should include the name of growth hormone, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple growth hormone regimens|


## herbicide_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|herbicide regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|atrazine;10 milligram per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000561|
|**Definition**|Information about treatment involving use of herbicides; information about treatment involving use of growth hormones; should include the name of herbicide, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple regimens|


## host_age
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host age|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|10 days|
|**Preferred unit**|year, day, hour|
|**URL**|https://w3id.org/mixs/terms/0000255|
|**Definition**|Age of host at the time of sampling; relevant scale depends on species and study, e.g. Could be seconds for amoebae or centuries for trees|


## host_common_name
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host common name|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|rice|


## host_disease_stat
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host disease status|
|**Requirement**|Optional|
|**Value syntax**|{text} \| {termLabel} {[termID]}|
|**Example**|downy mildew|


## host_dry_mass
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host dry mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000257|
|**Definition**|Measurement of dry mass|


## host_genotype
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host genotype|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Ts|


## host_height
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host height|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000264|
|**Definition**|The height of subject|


## host_length
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host length|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1 meter|
|**Preferred unit**|centimeter, millimeter, meter|
|**URL**|https://w3id.org/mixs/terms/0000256|
|**Definition**|The length of subject.|


## host_life_stage
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host life stage|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|adult|


## host_phenotype
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host phenotype|
|**Requirement**|Optional|
|**Value syntax**|{text};{termLabel} {[termID]}|
|**Example**|seed pod; green [PATO:0000320]|


## host_subspecf_genlin
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host subspecific genetic lineage|
|**Requirement**|Optional|
|**Value syntax**|{rank name}:{text}|
|**Example**|subvariety:glabrum|


## host_symbiont
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|observed host symbionts|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|flukeworms|


## host_taxid
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host taxid|
|**Requirement**|Optional|
|**Value syntax**|{NCBI taxid}|
|**Example**|4530|


## host_tot_mass
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host total mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|2500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000263|
|**Definition**|Total mass of the host at collection, the unit depends on host|


## host_wet_mass
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|host wet mass|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 gram|
|**Preferred unit**|kilogram, gram|
|**URL**|https://w3id.org/mixs/terms/0000567|
|**Definition**|Measurement of wet mass|


## humidity_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|humidity regimen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|25 gram per cubic meter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram per cubic meter|
|**URL**|https://w3id.org/mixs/terms/0000568|
|**Definition**|Information about treatment involving an exposure to varying degree of humidity; information about treatment involving use of growth hormones; should include amount of humidity administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple regimens|


## light_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|light regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{float} {unit}|
|**Example**|incandescant light;10 lux;450 nanometer|
|**Preferred unit**|lux; micrometer, nanometer, angstrom|
|**URL**|https://w3id.org/mixs/terms/0000569|
|**Definition**|Information about treatment(s) involving exposure to light, including both light intensity and quality.|


## mechanical_damage
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|mechanical damage|
|**Requirement**|Optional|
|**Value syntax**|{text};{text}|
|**Example**|pruning;bark|


## mineral_nutr_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|mineral nutrient regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|potassium;15 gram;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000570|
|**Definition**|Information about treatment involving the use of mineral supplements; should include the name of mineral nutrient, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple mineral nutrient regimens|


## misc_param
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## misc_param
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|miscellaneous parameter|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|Bicarbonate ion concentration;2075 micromole per kilogram|


## non_min_nutr_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|non-mineral nutrient regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|carbon dioxide;10 mole per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000571|
|**Definition**|Information about treatment involving the exposure of plant to non-mineral nutrient such as oxygen, hydrogen or carbon; should include the name of non-mineral nutrient, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple non-mineral nutrient regimens|


## organism_count
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|organism count|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};[qPCR \| ATP \| MPN \| other]|
|**Example**|total prokaryotes;3.5e7 cells per milliliter;qPCR|
|**Preferred unit**|number of cells per cubic meter, number of cells per milliliter, number of cells per cubic centimeter|
|**URL**|https://w3id.org/mixs/terms/0000103|
|**Definition**|Total cell count of any organism (or group of organisms) per gram, volume or area of sample, should include name of organism followed by count. The method that was used for the enumeration (e.g. qPCR, atp, mpn, etc.) Should also be provided. (example: total prokaryotes; 3.5e7 cells per ml; qpcr)|


## oxy_stat_samp
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|oxygenation status of sample|
|**Requirement**|Optional|
|**Value syntax**|[aerobic \| anaerobic \| other]|
|**Example**|aerobic|


## perturbation
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|perturbation|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|antibiotic addition;R2/2018-05-11T14:30Z/2018-05-11T19:30Z/P1H30M|


## pesticide_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|pesticide regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|pyrethrum;0.6 milligram per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, mole per liter, milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000573|
|**Definition**|Information about treatment involving use of insecticides; should include the name of pesticide, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple pesticide regimens|


## ph_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|pH regimen|
|**Requirement**|Optional|
|**Value syntax**|{float};{Rn/start_time/end_time/duration}|
|**Example**|7.6;R2/2018-05-11:T14:30/2018-05-11T19:30/P1H30M|


## plant_growth_med
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|plant growth medium|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]} or [husk \| other artificial liquid medium \| other artificial solid medium \| peat moss \| perlite \| pumice \| sand \| soil \| vermiculite \| water]|
|**Example**|hydroponic plant culture media [EO:0007067]|


## plant_product
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|plant product|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|xylem sap [PO:0025539]|


## plant_sex
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|plant sex|
|**Requirement**|Optional|
|**Value syntax**|[Androdioecious \| Androecious \| Androgynous \| Androgynomonoecious \| Andromonoecious \| Bisexual \| Dichogamous \| Diclinous \| Dioecious \| Gynodioecious \| Gynoecious \| Gynomonoecious \| Hermaphroditic \| Imperfect \| Monoclinous \| Monoecious \| Perfect \| Polygamodioecious \| Polygamomonoecious \| Polygamous \| Protandrous \| Protogynous \| Subandroecious \| Subdioecious \| Subgynoecious \| Synoecious \| Trimonoecious \| Trioecious \| Unisexual]|
|**Example**|Hermaphroditic|


## plant_struc
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|plant structure|
|**Requirement**|Optional|
|**Value syntax**|{termLabel} {[termID]}|
|**Example**|epidermis [PO:0005679]|


## radiation_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|radiation regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|gamma radiation;60 gray;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|rad, gray|
|**URL**|https://w3id.org/mixs/terms/0000575|
|**Definition**|Information about treatment involving exposure of plant or a plant part to a particular radiation regimen; should include the radiation type, amount or intensity administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple radiation regimens|


## rainfall_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rainfall regimen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|15 millimeter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|millimeter|
|**URL**|https://w3id.org/mixs/terms/0000576|
|**Definition**|Information about treatment involving an exposure to a given amount of rainfall, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple regimens|


## root_cond
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting conditions|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|http://himedialabs.com/TD/PT158.pdf|


## root_med_carbon
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium carbon|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|sucrose|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000577|
|**Definition**|Source of organic carbon in the culture rooting medium; e.g. sucrose.|


## root_med_macronutr
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium macronutrients|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|KH2PO4;170¬†milligram per liter|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000578|
|**Definition**|Measurement of the culture rooting medium macronutrients (N,P, K, Ca, Mg, S); e.g. KH2PO4 (170¬†mg/L).|


## root_med_micronutr
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium micronutrients|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|H3BO3;6.2¬†milligram per liter|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000579|
|**Definition**|Measurement of the culture rooting medium micronutrients (Fe, Mn, Zn, B, Cu, Mo); e.g. H3BO3 (6.2¬†mg/L).|


## root_med_ph
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium pH|
|**Requirement**|Optional|
|**Value syntax**|{float}|
|**Example**|7.5|


## root_med_regl
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium regulators|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|abscisic acid;0.75 milligram per liter|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000581|
|**Definition**|Growth regulators in the culture rooting medium such as cytokinins, auxins, gybberellins, abscisic acid; e.g. 0.5¬†mg/L NAA.|


## root_med_solid
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium solidifier|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|agar|


## root_med_suppl
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|rooting medium organic supplements|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit}|
|**Example**|nicotinic acid;0.5 milligram per liter|
|**Preferred unit**|milligram per liter|
|**URL**|https://w3id.org/mixs/terms/0000580|
|**Definition**|Organic supplements of the culture rooting medium, such as vitamins, amino acids, organic acids, antibiotics activated charcoal; e.g. nicotinic acid (0.5¬†mg/L).|


## salinity
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|salinity|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 practical salinity unit|
|**Preferred unit**|practical salinity unit, percentage|
|**URL**|https://w3id.org/mixs/terms/0000183|
|**Definition**|The total concentration of all dissolved salts in a liquid or solid sample. While salinity can be measured by a complete chemical analysis, this method is difficult and time consuming. More often, it is instead derived from the conductivity measurement. This is known as practical salinity. These derivations compare the specific conductance of the sample to a salinity standard such as seawater.|


## salt_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|salt regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|NaCl;5 gram per liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|gram, microgram, mole per liter, gram per liter|
|**URL**|https://w3id.org/mixs/terms/0000582|
|**Definition**|Information about treatment involving use of salts as supplement to liquid and soil growth media; should include the name of salt, amount administered, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple salt regimens|


## samp_capt_status
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|sample capture status|
|**Requirement**|Optional|
|**Value syntax**|[active surveillance in response to an outbreak \| active surveillance not initiated by an outbreak \| farm sample \| market sample \| other]|
|**Example**|farm sample|


## samp_dis_stage
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|sample disease stage|
|**Requirement**|Optional|
|**Value syntax**|[dissemination \| growth and reproduction \| infection \| inoculation \| penetration \| other]|
|**Example**|infection|


## samp_store_dur
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|sample storage duration|
|**Requirement**|Optional|
|**Value syntax**|{duration}|
|**Example**|P1Y6M|


## samp_store_loc
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|sample storage location|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|Freezer no:5|


## samp_store_temp
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|sample storage temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|-80 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000110|
|**Definition**|Temperature at which sample was stored, e.g. -80 degree Celsius|


## samp_vol_we_dna_ext
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|sample volume or weight for DNA extraction|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|1500 milliliter|
|**Preferred unit**|millliter, gram, milligram, square centimeter|
|**URL**|https://w3id.org/mixs/terms/0000111|
|**Definition**|Volume (ml) or mass (g) of total collected sample processed for DNA extraction. Note: total sample collected should be entered under the term Sample Size (MIXS:0000001).|


## season_environment
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|seasonal environment|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|rainy;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|


## standing_water_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|standing water regimen|
|**Requirement**|Optional|
|**Value syntax**|{text};{Rn/start_time/end_time/duration}|
|**Example**|standing water;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|


## temp
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|temperature|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit}|
|**Example**|25 degree Celsius|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000113|
|**Definition**|Temperature of the sample at the time of sampling|


## tiss_cult_growth_med
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|tissue culture growth media|
|**Requirement**|Optional|
|**Value syntax**|{PMID} \| {DOI} \| {URL} \| {text}|
|**Example**|https://link.springer.com/content/pdf/10.1007/BF02796489.pdf|


## water_temp_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|water temperature regimen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|15 degree Celsius;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|degree Celsius|
|**URL**|https://w3id.org/mixs/terms/0000590|
|**Definition**|Information about treatment involving an exposure to water with varying degree of temperature, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple regimens|


## watering_regm
|||
|---|---|
|**Package**|plant-associated|
|**Item (rdfs:label)**|watering regimen|
|**Requirement**|Optional|
|**Value syntax**|{float} {unit};{Rn/start_time/end_time/duration}|
|**Example**|1 liter;R2/2018-05-11T14:30/2018-05-11T19:30/P1H30M|
|**Preferred unit**|milliliter, liter|
|**URL**|https://w3id.org/mixs/terms/0000591|
|**Definition**|Information about treatment involving an exposure to watering frequencies, treatment regimen including how many times the treatment was repeated, how long each treatment lasted, and the start and end time of the entire treatment; can include multiple regimens|
