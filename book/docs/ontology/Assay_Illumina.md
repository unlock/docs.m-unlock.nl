# Assay Illumina

## FileNameForward
|||
|---|---|
|**Package**|Illumina|
|**Item (rdfs:label)**|Forward filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_1.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|


## FileNameReverse
|||
|---|---|
|**Package**|Illumina|
|**Item (rdfs:label)**|Reverse filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|NG-13425_Fyig_005_lib124679_5331_4_2.fastq.gz|
|**URL**|http://m-unlock.nl/ontology/file|
