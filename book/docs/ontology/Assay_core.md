# Assay core

## AssayIdentifier
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Assay identifier|
|**Requirement**|Mandatory|
|**Value syntax**|{id}{5,25}|
|**Example**|AssayId|
|**URL**|http://schema.org/identifier|


## AssayName
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Assay name|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{10,}|
|**Example**|Assay name|
|**URL**|http://schema.org/name|


## AssayDescription
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Assay description|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{25,}|
|**Example**|assay description|
|**URL**|http://schema.org/description|


## IsolationProtocol
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Isolation protocol|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|http://example.com|


## Facility
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Facility|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|facility name|


## Platform
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Platform|
|**Requirement**|Mandatory|
|**Value syntax**|(Illumina \| PacBio \| Nanopore)|
|**Example**|Illumina|


## InstrumentModel
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Instrument model|
|**Requirement**|Mandatory|
|**Value syntax**|{text}|
|**Example**|Illumina HiSeq|


## Date
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Date|
|**Requirement**|Mandatory|
|**Value syntax**|{timestamp}|
|**Example**|2018-06-03 00:00:00|
|**URL**|https://schema.org/dateCreated|


## SameAs
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Same as|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|P12353|
