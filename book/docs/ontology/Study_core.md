# Study core

## StudyIdentifier
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Study identifier|
|**Requirement**|Mandatory|
|**Value syntax**|{id}{5,25}|
|**Example**|StudyX|
|**URL**|http://schema.org/identifier|
|**Definition**|Identifier corresponding to the study|


## StudyTitle
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Study title|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{10,}|
|**Example**|Title of the study|
|**URL**|http://schema.org/title|
|**Definition**|Title describing the study|


## StudyDescription
|||
|---|---|
|**Package**|core|
|**Item (rdfs:label)**|Study description|
|**Requirement**|Mandatory|
|**Value syntax**|{text}{50,}|
|**Example**|Description of the study|
|**URL**|http://schema.org/description|
|**Definition**|Description of the study|
