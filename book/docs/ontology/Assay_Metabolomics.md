# Assay Metabolomics

## FileName
|||
|---|---|
|**Package**|Metabolomics|
|**Item (rdfs:label)**|Filename|
|**Requirement**|Optional|
|**Value syntax**|{text}|
|**Example**|filex.gz|
|**URL**|http://m-unlock.nl/ontology/file|
