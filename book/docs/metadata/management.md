# Management

*Important when you are running your own instance of the FAIR Data Station*

An important part of the metadata registration form is the metadata.xlsx file that is incorporated into the resource (src/main/resources/metadata.xlsx). The current version can be found [here](https://gitlab.com/m-unlock/fairds/-/blob/master/src/main/resources/metadata.xlsx).
 This excel file contains the elements that are optional or required for the different levels (Project, Investigation, etc...). Each level contains some obligatory elements such as the identifier or description of an object and can easily be modified as indicated by the Requirement column.

Each sheet contains the following columns:

```
# Package: makes a distinction between different metadata packages available (Currently only supported for observation unit, sample and assay types)
Structured comment name: The identifier of the property (e.g., carb_dioxide)
Item (rdfs:label): A human readable label which is used in the excel headers and in the ontology as rdfs:label for the properties
Requirement: Mandatory (M) or Optional (X)
Value syntax: The format used (numeric, data, string, unit) validated through regular expressions (e.g., {float} {unit})
Example: An example of the value and how it is defined (e.g., 410 parts per million)
URL: RDF property URL when defined otherwise will switch to default URL space + /<structured comment name>
Preferred unit: The unit of measurement that is preferred but not obligatory
Definition: The definition of the structured comment name
```

The sheets, Project, Investigation, Study, ObservationUnit, Sample and Assay are obligatory sheets the content of the optional properties can be freely adjusted. New ObservationUnit, Sample and Assay types can be created by creating a new row with a different `# Package name`. These packages are an extension on the core package which contains elements that are shared among the packages defined.

## Metadata syntax

There is support for a large number of different types and general regular expressions. A subset of the complete metadata table used in the FAIR Data Station can be found below.

When the syntax of certain properties are too specific or too generic they can be easily modified accordingly. When you are building your own FAIR Data Station you can modify the excel file in the resource folder. If you only work with the metadata file you can use the precompiled java program and upon execution the metadata.xlsx becomes available next to the application. When you modify the excel file and you restart the program the Metadata fields will be updated accordingly and can be used for the generation of the templates and for the validation of the metadata excel files.

| **Metadata syntax**                                      | **Description** |
|----------------------------------------------------------|-----------------|
| (1st floor\|2nd floor\|{integer} floor\|basement\|lobby) | An or list combined with an integer possibility.                |
| (Daily\|Weekly\|Monthly\|Quarterly\|Annually\|other)     | A generic or list with all predefined variables                |
| {NCBI taxid}                                             | A numeric field                |
| {PMID}\|{DOI}\|{URL}\|{text}                             | URL's representing a specific address                |
| {boolean}                                                | Standard boolean field                |
| {boolean},{integer}                                      | Boolean combined with an integer                |
| {boolean};{timestamp}                                    | Boolean combined with a timestamp                |
| {dna}                                                    | The alphabet used for DNA representation                |
| {file}                                                   | Represents a file object based on a filename                |
| {float}                                                  | Generic decimal value                |
| {float} \- {float} {unit}                                | Combination of a decimal and / or a unit value                |
| {float} {unit};[GL\|DF\|RT\|KB\|MSL\|other]              | A decimal unit combined with a list of elements                |
| {id}{5,25}                                               | An identifier (string) with a range length                |
| {integer}                                                | Numeric value                |
| {integer} \- {integer} {unit}                            | Number or numeric with a unit                |
| {percentage}                                             | Percentage value                |
| {rank name}:{text}                                       | Text combination                |
| {term}                                                   | Text representing a term                |
| {text} {integer}                                         | Text representing a term and corresponding numeric value                |
| {text}{10,}                                              | Text with a minimum length                |
| {timestamp}                                              | The timestamp value                |