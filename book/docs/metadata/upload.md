# Validation

To validate your excel file you can upload it at https://data.m-unlock.nl/upload. 
This will validate your file and will show you if there are any incosistencies. 

![](../figures/upload.png)

After validation and if you filled in everything correctly you should see a report summary of the validation process.
In addition you can download the metadata file as RDF.

If the validation was unsuccesful please check the error message carfully and see if you can identify the problem. 
If not feel free to contact us to help you out.

