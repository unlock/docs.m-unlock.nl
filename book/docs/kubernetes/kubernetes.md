# Kubernetes

## Requirements

This documentation is currently written for a barebone Ubuntu 18.04.5 LTS system.

- Docker, please use the official installation procedures at https://docs.docker.com/get-docker/


### Starting from scratch

- To start the kubernetes manager on the master node

`sudo docker run --privileged -d --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher`

- In this case we create / manage a multi cluster setup with a custom new password

![](welcome.png)

- Set the server URL 

![](serverurl.png)

- You should arrive on the overview page in which you can create a new cluster

![](addcluster.png)


When working with a local instance select the `Create a new Kubernetes cluster option`
Now you can setup the name of the cluster and when needed change additional parameters (you can leave it to default).

![](newcluster.png)

After the creation of the cluster you have the option to add nodes. 

![](addnode.png)

You need to have at least a etcd/control plane node and for the actual work you need the worker nodes. You can copy the command and execute it on the node via the terminal.

If all went well you should be able to see the nodes becoming active in the node overview.

![](nodes.png)

### Namespaces

The unlock jobs will remain within the unlock project space in rancher. Go to Projects/Namespaces and create a new project. Add members to the project if needed. Also make sure you create the namespace for unlock (Click Namespaces when in the unlock project).

### Obtaining the Kubeconfig file

When the cluster is running and you would like to access the cluster from a local machine  you need to obtain the kubeconfig file. This file can easily be obtained when you  have selected the cluster.

![](kubeconfig.png)


### Adding storage

Within the unlock infrastructure each node has the databases and applications needed for the analysis to be performed. This folder needs to be accessible from the docker image that will execute the workflow. To achive this a host path is added to the configuration.

Go to storage > persistent volumes

![](storage.png)

In addition make sure that the volume is also added to the specific project.

![](volumes.png)


### Adding secrets

Make sure you are inside the unlock namespace/project. When you are within the project you should be able to see the Resource tab which contains a Secrets section.

![](secrets.png)

### Adding a ConfigMap
Environmental (non-secret!) variables can be set using a ConfigMap for example the PATH variable. When you are within a namespace you should be able to see the Resource tab which contains a Config section. Here you can set the name of your ConfigMap and a Key-Value pair.

![](configmap.png)

And add it to your pod yaml container section:
```
envFrom:
- configMapRef:
    name: path
```
