## WebDav

It is possible to use webdav in iRODS by placing the ***URL*** that is normally used for the authentication procedure in your web browser. It should prompt a username / password panel which when succesfully will show you the iRODS instance content.

It is also possible to mount the iRODS instance as a network drive. Select the tab that corresponds to your environment for more details.

=== "Mac"
    On a mac you can mount webdav folders using the Finder. When you have a Finder window open press `CMD + k`. This will provide a popup in which you can provide the webdav URL.

    ![](images/webdav_mac.png)

    When prompted for a username / password provide the credentials used to access the webdav system.

=== "Linux/Ubuntu"
    In linux you can mount other locations such as webdav using your file browser. Simply click on 'Other Locations' and provide the address of your webdav server. Note, if the webdav server uses SSL-encryption you might have to use the protocol *davs*, otherwise use *dav*:

    ![](images/DavRodsMount-linux2.png)
    
    When prompted for a username / password, please provide your credentials for the webdav/iRODS system.
    

=== "Windows"
    When using windows, you have to mount the WebDAV system as a network drive.
    
    - Start your file browser
    
    - Right click on the computer icon

    - Map network drive

    - Paste the entire WebDAV URL in the Folder

    ![](images/webdav_windows.png)

    - Click finish and when requested for your credentials use the credentials to access the webdav system.



=== "Cyberduck"

    There are other applications which you can use to connect to the webdav endpoint such as for example Cyberduck.

    - Paste the webdav address in the Quick connect bar and it should connect to your webdav instance.

    - When requested for your credentials use the credentials to access the webdav system.
    
    ![Cyberduck interface using the webdav protocol](images/cyberduck.png)

## iCommands

Currently the latest version of iCommands is only available through a Linux environment.
The easiest approach to have icommands available on your system is through Docker.

To install docker look at your operating system below:


=== "Windows 10"

    ```
	Windows 10 docker installation instructions

	0. Administrator rights are required for this procedure. If you do not have these, please request them by sending an email to servicedesk.it@wur.nl
	1. Open a Windows powershell window as an administrator. Run the following command to enable the Windows subsystem for linux:
	`Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux`. __Reboot as required.__
	2. Open the Microsoft store by opening or searching for it via the start menu. In the store search for Ubuntu. Select the "Ubuntu 18.04" app and download it.
	3. After downloading and installing, the app should be ready to go. Start it via the start menu by clicking on the tile / searching for it. The app will continue installing. After that, create a username/ password according to the instructions that appear.
	4. You are now at a Linux terminal. You can follow the [general iRods tutorial](https://github.com/irods/irods_training/raw/master/beginner/irods_beginner_training_2019.pdf) from this point on (start from chapter 4 to get practical).
    ```

=== "Mac OSX"

    ```
    To install docker for mac please download and install the docker application from https://www.docker.com/products/docker-desktop

    When docker is installed we have prepared a docker image    with the iCommands.
    ```

=== "Ubuntu"

    ```
    It is possible to install the iCommands directly within ubuntu. 

    To install docker for ubuntu please follow the
    instructions on this website: 
    https://docs.docker.com/engine/install/ubuntu/
    ```

### Docker
-----

To start the docker container with the icommands available you can use this command:

```
docker run -it registry.gitlab.com/digitaltwins/irods:latest
```

As this is a clean docker instance it does not contain any authentication documentation. You can create the required irods_environment.json file inside the docker instance but upon starting a new instance this information is lost.

The best approach would be to exit the docker container and create an irods folder containing the irods_envrionment.json file. There are also additional features within this json file that ensures that your connection is encrypted and that you do not need to provide all access information each time you start the docker container.

- Create an `irods` folder
- Create an irods_environment.json file in the `irods` folder with the following content

```
{
    "irods_user_name": "USERNAME",
    "irods_zone_name": "ZONE",
    "irods_host": "HOSTNAME",
    "irods_port": 1247,
    "irods_client_server_negotiation": "request_server_negotiation",
    "irods_client_server_policy": "CS_NEG_REQUIRE",
    "irods_encryption_key_size": 32,
    "irods_encryption_salt_size": 8,
    "irods_encryption_num_hash_rounds": 16,
    "irods_encryption_algorithm": "AES-256-CBC"
}
```

Make sure that you change your USERNAME, ZONE and HOSTNAME to the correct values.

When starting the docker container it is essential that the irods folder is mounted to the internal location of `~/irods/`. The mounting of the folder is achieved via the `-v` parameter.

```
docker run -it -v /PATH/TO/IRODS/FOLDER:/root/.irods registry.gitlab.com/digitaltwins/irods:latest
```

When the docker container has succesfully started you can login via the `iinit command`

```
> iinit
Enter your current iRODS password:
```

When that is succcessful the iCommands are available for use. See `ihelp` or for more information.

## Python

## Accessing only data in iRODS

### iRODS tickets

## Publishing data with iRODS

## Anonymous access
