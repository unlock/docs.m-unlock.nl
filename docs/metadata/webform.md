# Web Form interface

As explained in the [Procedure and registration section](../Procedure/), there is a *Metadata Registration Web Form* to create a template Excel sheet to create a Project and add data to your Project to use the UNLOCK E-Infrastructure.

You can access the Web form https://data.m-unlock.nl/ and click on the `Metadata Template` tab.

This will give an interface that looks like this:

![Web Interface](./img/web.png)

The *Metadata Registration Web Form* is divided in to different segments; Project, Investigation,Study, etc.. You can see the relations between these segments in Figure below.

An explanation of each segment can be found on this page. And also what we expect it to contain.

##### **The Unlock Project data management structure**

![UNLOCK datamanagement](../figures/unlock_isa_2.png)

### Project

A Project are the overarching research programmes with defined aims.
For example it could encompass a grant-funded programme of work. All data and metadata belongs to a Project.

**Required fields**

Project Identifier: At least 5 characters max 25 characters.
Project Title: At least 10 characters.
Project Description: At least 50 characters.

Users specific fields can be added using the Excel sheet. See [Metadata Excel](../Metadata/Excel) how this works.

Example of the Project section

![Web Interface](./img/project.png)

### Investigation

Investigations are defined here as research questions within the the specified project. The Web form is designed to enter a single investigation. 

**Investigations** belong to a **Project**.

Example of the Investigation section

![Web Interface](./img/investigation.png)


### Study

A study comprises a series of observation units (see below) and assays or measurements of one or more types, undertaken to answer a particular biological question within it's investigation.

Every **Study** has an associated **Investigation**

Example of the Study section

![Web Interface](./img/study.png)

  

### Observation Unit

Observation units are objects that are subject to instances of observation and measurement.

Every **Observation Unit** is associated with a **Study**

 For example in a study each entity patient, plant, animal, bioreactor or area studied is one observation unit. When studying 50 different animals, each animal should become one observation unit. Additional packages will be released that support different observation unit types such as bioreactors, patients, animals, fields or oceans.

Example of the Observation Unit section

![Web Interface](./img/observation_unit.png)

  

### Sample

A **Sample** is taken from an **Observation Unit** that can potentially be processed further to acquire data from. 
 A multitude of samples can be taken from one or more observation units when for example performing time-scale 
 experiments or sampling from different regions of the observation unit.

A varity of sample packages are available and are based on the MIxS standard (https://gensc.org/mixs/). Each package has its own obligatory fields and varying additional fields depending on the sample type.

Every **Sample** belongs to an **Observation Unit**

Example of the Sample section

![Web Interface](./img/sample.png)
 

### Assays

An **Assay** corresponds to the data (for example a sequencing run) that was performed on a sample obtained from its observation unit.

 You have to enable the assay section (dna/rna/amplicon) to enable to column selection. When you have multiple types of assays, for example DNA and RNA, you can enable both sheets and appropriate rows. In addition, columns can be adding using this form when such information is available. 

Example of the DNA assay section

![Web Interface](./img/assay.png)![Web Interface](./img/assay2.png)

