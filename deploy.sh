#!/bin/bash
#=======================================================================================
#title          :Unlock docs deploy
#description    :Deploy Unlock Documentation including metadata fields and ontology docs
#author         :Bart Nijsse & Jasper Koehorst
#date           :2021
#version        :0.0.1
#=======================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Build metadata docs
if ! [ -d $DIR/metadata_docs ]; then
  git clone https://git.wur.nl/unlock/metadata_docs.git
fi

cd $DIR/metadata_docs
git pull
./build.sh $DIR/metadatafields/unlock_metadata.xlsx

# Build ontology
cd $DIR
if ! [ -d $DIR/ontology ]; then
  git clone https://git.wur.nl/unlock/ontology.git
fi
git pull
cd $DIR/ontology/unlock_docs
./build.sh

#Build docs.m-unlock.nl
cd $DIR
mkdocs build

# add ontology docs
cp -r $DIR/ontology/unlock_docs/site $DIR/site/ontology

# add metadata docs
cp -r $DIR/metadatafields $DIR/site/
cp -r $DIR/metadata_docs/site/* $DIR/site/metadatafields/

# make backup and deploy
if [ -d $DIR/unlockdocs ]; then
  rm -rf unlockdocs.bck
  mv unlockdocs unlockdocs.bck
fi
mv $DIR/site $DIR/unlockdocs